﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeywordManager.Components
{
    public class ConfirmBase : ComponentBase
    {
        protected bool ShowConfirmation { get; set; }

        [Parameter]
        public string ConfirmationTitle { get; set; } = "title";

        [Parameter]
        public string ConfirmationMessage { get; set; } = "message";

        public string captionCancel { get; set; } = "cancel";
        public bool showBtnChange { get; set; } = true;

        public Action action { get; set; } = null;

        public void Show()
        {
            ShowConfirmation = true;
            StateHasChanged();
        }

        [Parameter]
        public EventCallback<bool> ConfirmationChanged { get; set; }

        protected async Task OnConfirmationChange(bool value)
        {
            if (action != null)
            {
                action();
                action = null;
            }
                

            ShowConfirmation = false;
            await ConfirmationChanged.InvokeAsync(value);
        }
    }
}
