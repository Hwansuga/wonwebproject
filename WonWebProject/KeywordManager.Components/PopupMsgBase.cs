﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Html;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeywordManager.Components
{
    public class PopupMsgBase : ComponentBase
    {
        protected bool show { get; set; } = false;
        public bool doneToLoad { get; set; } = true;
        public string title { get; set; } = "";
        public MarkupString msg { get; set; } = new MarkupString("");
        public string captionOk { get; set; } = "Ok";
        public string captinoCancel { get; set; } = "Cancel";

        public bool visibleOk { get; set; } = true;
        public bool visibleCancel { get; set; } = true;


        public Func<Task> funcOk = null;

        public void SetShow()
        {
            show = true;
            StateHasChanged();
        }

        public async Task OnclickOK()
        {
            doneToLoad = false;
            StateHasChanged();

            if (funcOk != null)
            {
                await funcOk();
            }
            else
            {
                await Task.Delay(1);
            }
            
            doneToLoad = true;
            show = false;
        }

        public void OnclickCancel()
        {
            funcOk = null;
            show = false;
        }
    }
}
