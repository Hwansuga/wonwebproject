﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ApiTestClient
{
    class Program
    {
        static void Main(string[] args)
        {
            //가장 적게 작업된 상품 정보 가져오기
            var item = GetLowWokingItem(5).GetAwaiter().GetResult();
            //var item = GetItemsByTag("nts1").GetAwaiter().GetResult();


            //var item = GetAllItems().GetAwaiter().GetResult();

            //상품 정보에 랭킹정보를 기업 후 서버에 갱신 요청
            JArray jarr = JArray.Parse(item);
            if (jarr.Count > 0)
            {
                JObject jobj = JObject.Parse(jarr[0].ToString());
                jobj["state"] = "이상함";
                //랭킹 정보 수정 api
                var updateRet = UpdateRankInfo(jobj.ToString(Newtonsoft.Json.Formatting.None)).GetAwaiter().GetResult();
                //var id = jobj["id"].ToString();
                //동작 횟수 올리는 api
                //var retCntWork = UpdateCntWork(id);
            }
            

            //ClearTag("").GetAwaiter().GetResult();
        }

        static HttpClient CreateApiClient()
        {
            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
            HttpClient client = new HttpClient(httpClientHandler);

            //client.BaseAddress = new Uri("Https://121.167.252.238:44397/");
            client.BaseAddress = new Uri("Https://localhost:44397/");
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkFkbWluVG9rZW4iLCJuYmYiOjE2MjU1NDM0NzcsImV4cCI6MTk0MTA3NjI3NywiaWF0IjoxNjI1NTQzNDc3fQ.gg3GCJIJGB3PJlNaHEQnD4Xk9tLqFoWxJGH4DV-avyk");

            return client;
        }

        static async Task<string> GetAllItems()
        {
            var hc = CreateApiClient();
            try
            {
                var encodedContent = new FormUrlEncodedContent(new Dictionary<string, string>());
                var response = await hc.PostAsync($"api/Goods/GetAllItems", encodedContent);
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        static async Task<string> ClearTag(string _tag)
        {
            var hc = CreateApiClient();
            try
            {
                var encodedContent = new FormUrlEncodedContent(new Dictionary<string, string>());
                var response = await hc.PutAsync($"api/Slot/ClearTag?_tag={_tag}", encodedContent);
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        static async Task<string> GetItemsByTag(string _tag)
        {
            var hc = CreateApiClient();
            try
            {
                var encodedContent = new FormUrlEncodedContent(new Dictionary<string, string>());
                var response = await hc.PostAsync($"api/Goods/GetItemsByTag?_tag={_tag}", encodedContent);
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }



        static async Task<string> GetLowWokingItem(int cnt_ = 1)
        {
            var hc = CreateApiClient();
            try
            {
                var encodedContent = new FormUrlEncodedContent(new Dictionary<string , string>());
                var response = await hc.PostAsync($"api/Goods/GetLowWokingItem?cnt={cnt_}", encodedContent);
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {
                throw;
            } 
        }

        static async Task<string> UpdateRankInfo(string data)
        {
            var hc = CreateApiClient();
            try
            {
                StringContent content = new StringContent(data , Encoding.UTF8 , "application/json");

                var response = await hc.PutAsync("api/Goods/UpdateRankInfo" , content);
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        static async Task<string> UpdateCntWork(string _id)
        {
            var hc = CreateApiClient();
            try
            {
                var encodedContent = new FormUrlEncodedContent(new Dictionary<string, string>());
                var response = await hc.PutAsync($"api/Slot/UpdateCntWork?_id={_id}" , encodedContent);
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
