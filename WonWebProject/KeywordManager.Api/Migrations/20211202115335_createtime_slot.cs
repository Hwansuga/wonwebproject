﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class createtime_slot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "fef26708-40e6-4f24-b2ea-cf856ce90cf7");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "4f0be4a8-ab88-4522-a310-c5af8153991d");

            migrationBuilder.AddColumn<DateTime>(
                name: "createTime",
                table: "slotDB",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "e8ee3655-d0ac-4612-a59e-3120b4c75a0f", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "createTime", "goodsId", "owner", "sendTimeStamp" },
                values: new object[] { "40671035-6df7-41d3-a1f7-70ea94b7a4c6", 0, 0, new DateTime(2021, 12, 2, 20, 53, 34, 933, DateTimeKind.Local).AddTicks(9999), "", "", new DateTime(2021, 12, 2, 0, 0, 0, 0, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "userPointDB",
                keyColumn: "id",
                keyValue: "test",
                column: "expire",
                value: new DateTime(2021, 12, 2, 0, 0, 0, 0, DateTimeKind.Local));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "e8ee3655-d0ac-4612-a59e-3120b4c75a0f");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "40671035-6df7-41d3-a1f7-70ea94b7a4c6");

            migrationBuilder.DropColumn(
                name: "createTime",
                table: "slotDB");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "fef26708-40e6-4f24-b2ea-cf856ce90cf7", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "goodsId", "owner", "sendTimeStamp" },
                values: new object[] { "4f0be4a8-ab88-4522-a310-c5af8153991d", 0, 0, "", "", new DateTime(2021, 11, 23, 0, 0, 0, 0, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "userPointDB",
                keyColumn: "id",
                keyValue: "test",
                column: "expire",
                value: new DateTime(2021, 11, 23, 0, 0, 0, 0, DateTimeKind.Local));
        }
    }
}
