﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class tagInSlot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "49fa0242-c57f-4333-9f83-9f0bdef19f63");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "cc19b73d-da08-4d95-8896-8e71ea9b8678");

            migrationBuilder.AddColumn<string>(
                name: "tag",
                table: "slotDB",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "bc111eb2-3525-4976-94db-100e965d067b", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "createTime", "goodsId", "owner", "sendTimeStamp", "tag" },
                values: new object[] { "d141a17a-ad34-4ff4-b785-f73e014de7cb", 0, 0, new DateTime(2022, 1, 5, 20, 14, 48, 807, DateTimeKind.Local).AddTicks(5022), "", "", new DateTime(2022, 1, 5, 0, 0, 0, 0, DateTimeKind.Local), "" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "bc111eb2-3525-4976-94db-100e965d067b");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "d141a17a-ad34-4ff4-b785-f73e014de7cb");

            migrationBuilder.DropColumn(
                name: "tag",
                table: "slotDB");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "49fa0242-c57f-4333-9f83-9f0bdef19f63", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "createTime", "goodsId", "owner", "sendTimeStamp" },
                values: new object[] { "cc19b73d-da08-4d95-8896-8e71ea9b8678", 0, 0, new DateTime(2022, 1, 5, 20, 8, 1, 842, DateTimeKind.Local).AddTicks(6848), "", "", new DateTime(2022, 1, 5, 0, 0, 0, 0, DateTimeKind.Local) });
        }
    }
}
