﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class InitGoods : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "goodsDB",
                columns: table => new
                {
                    id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    keyword = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    value1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    value2 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    option = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    rank1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    rank1Change = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    rank2 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    rank2Change = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    memo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    owner = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_goodsDB", x => x.id);
                });

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "1674968d-1932-4f93-960a-1897bc7394ec", "코인", "메모", null, null, "", "", "", "", null, null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "goodsDB");
        }
    }
}
