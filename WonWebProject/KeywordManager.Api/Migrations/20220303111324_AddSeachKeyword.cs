﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class AddSeachKeyword : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "9a594e31-4376-449e-9307-48fb018a992b");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "0f460217-f840-4fe4-ac3d-120e956d53d4");

            migrationBuilder.AddColumn<string>(
                name: "searchKeyword",
                table: "goodsDB",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "searchKeyword", "value1", "value2" },
                values: new object[] { "7b755fa1-6db2-4bea-83ec-817a4aa8c18a", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "createTime", "goodsId", "owner", "sendTimeStamp", "tag" },
                values: new object[] { "5bc896a7-dfb7-4059-8819-206ffab24724", 0, 0, new DateTime(2022, 3, 3, 20, 13, 24, 9, DateTimeKind.Local).AddTicks(8177), "", "", new DateTime(2022, 3, 3, 0, 0, 0, 0, DateTimeKind.Local), "" });

            migrationBuilder.UpdateData(
                table: "userPointDB",
                keyColumn: "id",
                keyValue: "test",
                column: "expire",
                value: new DateTime(2022, 3, 3, 0, 0, 0, 0, DateTimeKind.Local));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "7b755fa1-6db2-4bea-83ec-817a4aa8c18a");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "5bc896a7-dfb7-4059-8819-206ffab24724");

            migrationBuilder.DropColumn(
                name: "searchKeyword",
                table: "goodsDB");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "9a594e31-4376-449e-9307-48fb018a992b", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "createTime", "goodsId", "owner", "sendTimeStamp", "tag" },
                values: new object[] { "0f460217-f840-4fe4-ac3d-120e956d53d4", 0, 0, new DateTime(2022, 2, 21, 12, 30, 31, 779, DateTimeKind.Local).AddTicks(8956), "", "", new DateTime(2022, 2, 21, 0, 0, 0, 0, DateTimeKind.Local), "" });

            migrationBuilder.UpdateData(
                table: "userPointDB",
                keyColumn: "id",
                keyValue: "test",
                column: "expire",
                value: new DateTime(2022, 2, 21, 0, 0, 0, 0, DateTimeKind.Local));
        }
    }
}
