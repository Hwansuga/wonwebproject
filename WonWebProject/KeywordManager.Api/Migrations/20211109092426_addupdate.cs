﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class addupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "9fdc69b4-3cd8-4925-9ed8-af338273757c");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "b9e63a46-3adf-4ca8-a600-e2806006251c");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "3f9bdc57-d341-4b87-9064-4a706c1aef12", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "create", "expire", "goodsId", "owner" },
                values: new object[] { "2965868e-3810-41f3-aeae-498b715481e5", 0, 0, new DateTime(2021, 11, 9, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2021, 11, 14, 0, 0, 0, 0, DateTimeKind.Local), "", "" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "3f9bdc57-d341-4b87-9064-4a706c1aef12");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "2965868e-3810-41f3-aeae-498b715481e5");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "9fdc69b4-3cd8-4925-9ed8-af338273757c", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "create", "expire", "goodsId", "owner" },
                values: new object[] { "b9e63a46-3adf-4ca8-a600-e2806006251c", 0, 0, new DateTime(2021, 7, 31, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2021, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "", "" });
        }
    }
}
