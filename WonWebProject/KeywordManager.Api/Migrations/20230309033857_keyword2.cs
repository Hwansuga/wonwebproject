﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class keyword2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "d58ade0d-7fc2-4637-ad0b-eda4203d1e5e");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "a5626023-9c8d-41fa-84a8-707c116735c4");

            migrationBuilder.AddColumn<string>(
                name: "id",
                table: "CKDB",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CKDB",
                table: "CKDB",
                column: "id");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "searchKeyword", "state", "value1", "value2" },
                values: new object[] { "3a28e28e-59a2-417f-a6d1-2e22b3a3f060", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "createTime", "goodsId", "owner", "sendTimeStamp", "tag" },
                values: new object[] { "900104e4-d97f-48fc-a1e2-502b0594e264", 0, 0, new DateTime(2023, 3, 9, 12, 38, 57, 123, DateTimeKind.Local).AddTicks(6786), "", "", new DateTime(2023, 3, 9, 0, 0, 0, 0, DateTimeKind.Local), "" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_CKDB",
                table: "CKDB");

            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "3a28e28e-59a2-417f-a6d1-2e22b3a3f060");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "900104e4-d97f-48fc-a1e2-502b0594e264");

            migrationBuilder.DropColumn(
                name: "id",
                table: "CKDB");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "searchKeyword", "state", "value1", "value2" },
                values: new object[] { "d58ade0d-7fc2-4637-ad0b-eda4203d1e5e", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "createTime", "goodsId", "owner", "sendTimeStamp", "tag" },
                values: new object[] { "a5626023-9c8d-41fa-84a8-707c116735c4", 0, 0, new DateTime(2023, 3, 9, 11, 41, 18, 423, DateTimeKind.Local).AddTicks(8588), "", "", new DateTime(2023, 3, 9, 0, 0, 0, 0, DateTimeKind.Local), "" });
        }
    }
}
