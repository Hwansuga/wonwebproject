﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class addWorkCntInSlot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "31c751b2-0419-4e77-a931-2292eb49e75e");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "a1fb1fba-0baf-46aa-8d97-cb30a98f7e23");

            migrationBuilder.AddColumn<int>(
                name: "cntRecv",
                table: "slotDB",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "cntSend",
                table: "slotDB",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "ba3b7c5f-d6fc-404b-8395-2ab1c8e2bd6d", 0, 0, "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "create", "expire", "goodsId", "owner" },
                values: new object[] { "561497a6-6f59-4685-937f-e881e7ecb047", 0, 0, new DateTime(2021, 7, 15, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2021, 7, 20, 0, 0, 0, 0, DateTimeKind.Local), "", "" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "ba3b7c5f-d6fc-404b-8395-2ab1c8e2bd6d");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "561497a6-6f59-4685-937f-e881e7ecb047");

            migrationBuilder.DropColumn(
                name: "cntRecv",
                table: "slotDB");

            migrationBuilder.DropColumn(
                name: "cntSend",
                table: "slotDB");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "31c751b2-0419-4e77-a931-2292eb49e75e", 0, 0, "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "create", "expire", "goodsId", "owner" },
                values: new object[] { "a1fb1fba-0baf-46aa-8d97-cb30a98f7e23", new DateTime(2021, 7, 15, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2021, 7, 20, 0, 0, 0, 0, DateTimeKind.Local), "", "" });
        }
    }
}
