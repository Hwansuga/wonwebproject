﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class goodscnt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "28ade4d6-d1fe-4512-8352-f7c89fd4cfbb");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "44c1ee3c-cb4e-4754-a9eb-c8c94ab87694");

            migrationBuilder.AddColumn<int>(
                name: "cntRecv",
                table: "goodsDB",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "cntSend",
                table: "goodsDB",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "58211c3b-e489-44d6-8017-2e046a142906", 0, 0, "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "create", "expire", "goodsId", "owner" },
                values: new object[] { "cb5b42d8-a5f2-4193-9547-de673c4c78e6", new DateTime(2021, 7, 11, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2021, 7, 16, 0, 0, 0, 0, DateTimeKind.Local), "", "" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "58211c3b-e489-44d6-8017-2e046a142906");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "cb5b42d8-a5f2-4193-9547-de673c4c78e6");

            migrationBuilder.DropColumn(
                name: "cntRecv",
                table: "goodsDB");

            migrationBuilder.DropColumn(
                name: "cntSend",
                table: "goodsDB");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "28ade4d6-d1fe-4512-8352-f7c89fd4cfbb", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "create", "expire", "goodsId", "owner" },
                values: new object[] { "44c1ee3c-cb4e-4754-a9eb-c8c94ab87694", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1, 1, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "", "" });
        }
    }
}
