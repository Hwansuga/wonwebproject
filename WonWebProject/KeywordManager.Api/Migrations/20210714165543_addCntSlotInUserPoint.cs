﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class addCntSlotInUserPoint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "58211c3b-e489-44d6-8017-2e046a142906");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "cb5b42d8-a5f2-4193-9547-de673c4c78e6");

            migrationBuilder.AddColumn<int>(
                name: "cntSlot",
                table: "userPointDB",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "31c751b2-0419-4e77-a931-2292eb49e75e", 0, 0, "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "create", "expire", "goodsId", "owner" },
                values: new object[] { "a1fb1fba-0baf-46aa-8d97-cb30a98f7e23", new DateTime(2021, 7, 15, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2021, 7, 20, 0, 0, 0, 0, DateTimeKind.Local), "", "" });

            migrationBuilder.UpdateData(
                table: "userPointDB",
                keyColumn: "id",
                keyValue: "test",
                column: "cntSlot",
                value: 1);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "31c751b2-0419-4e77-a931-2292eb49e75e");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "a1fb1fba-0baf-46aa-8d97-cb30a98f7e23");

            migrationBuilder.DropColumn(
                name: "cntSlot",
                table: "userPointDB");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "58211c3b-e489-44d6-8017-2e046a142906", 0, 0, "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "create", "expire", "goodsId", "owner" },
                values: new object[] { "cb5b42d8-a5f2-4193-9547-de673c4c78e6", new DateTime(2021, 7, 11, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2021, 7, 16, 0, 0, 0, 0, DateTimeKind.Local), "", "" });
        }
    }
}
