﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class addState : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "7b755fa1-6db2-4bea-83ec-817a4aa8c18a");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "5bc896a7-dfb7-4059-8819-206ffab24724");

            migrationBuilder.AddColumn<string>(
                name: "state",
                table: "goodsDB",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "searchKeyword", "state", "value1", "value2" },
                values: new object[] { "f7db0983-7e57-42e0-bbf7-8e36b204ee0c", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "createTime", "goodsId", "owner", "sendTimeStamp", "tag" },
                values: new object[] { "d0ecdf86-571a-4810-be1c-64ec4c292e3b", 0, 0, new DateTime(2022, 3, 21, 21, 10, 22, 816, DateTimeKind.Local).AddTicks(4598), "", "", new DateTime(2022, 3, 21, 0, 0, 0, 0, DateTimeKind.Local), "" });

            migrationBuilder.UpdateData(
                table: "userPointDB",
                keyColumn: "id",
                keyValue: "test",
                column: "expire",
                value: new DateTime(2022, 3, 21, 0, 0, 0, 0, DateTimeKind.Local));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "f7db0983-7e57-42e0-bbf7-8e36b204ee0c");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "d0ecdf86-571a-4810-be1c-64ec4c292e3b");

            migrationBuilder.DropColumn(
                name: "state",
                table: "goodsDB");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "searchKeyword", "value1", "value2" },
                values: new object[] { "7b755fa1-6db2-4bea-83ec-817a4aa8c18a", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "createTime", "goodsId", "owner", "sendTimeStamp", "tag" },
                values: new object[] { "5bc896a7-dfb7-4059-8819-206ffab24724", 0, 0, new DateTime(2022, 3, 3, 20, 13, 24, 9, DateTimeKind.Local).AddTicks(8177), "", "", new DateTime(2022, 3, 3, 0, 0, 0, 0, DateTimeKind.Local), "" });

            migrationBuilder.UpdateData(
                table: "userPointDB",
                keyColumn: "id",
                keyValue: "test",
                column: "expire",
                value: new DateTime(2022, 3, 3, 0, 0, 0, 0, DateTimeKind.Local));
        }
    }
}
