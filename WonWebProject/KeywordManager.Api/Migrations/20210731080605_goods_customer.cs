﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class goods_customer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "ba3b7c5f-d6fc-404b-8395-2ab1c8e2bd6d");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "561497a6-6f59-4685-937f-e881e7ecb047");

            migrationBuilder.AddColumn<string>(
                name: "customer",
                table: "goodsDB",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "goodsName",
                table: "goodsDB",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "9fdc69b4-3cd8-4925-9ed8-af338273757c", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "create", "expire", "goodsId", "owner" },
                values: new object[] { "b9e63a46-3adf-4ca8-a600-e2806006251c", 0, 0, new DateTime(2021, 7, 31, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2021, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "", "" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "9fdc69b4-3cd8-4925-9ed8-af338273757c");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "b9e63a46-3adf-4ca8-a600-e2806006251c");

            migrationBuilder.DropColumn(
                name: "customer",
                table: "goodsDB");

            migrationBuilder.DropColumn(
                name: "goodsName",
                table: "goodsDB");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "ba3b7c5f-d6fc-404b-8395-2ab1c8e2bd6d", 0, 0, "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "create", "expire", "goodsId", "owner" },
                values: new object[] { "561497a6-6f59-4685-937f-e881e7ecb047", 0, 0, new DateTime(2021, 7, 15, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2021, 7, 20, 0, 0, 0, 0, DateTimeKind.Local), "", "" });
        }
    }
}
