﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class Initstting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "settingDB",
                columns: table => new
                {
                    version = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    cntActionPerSlot = table.Column<int>(type: "int", nullable: false),
                    maxSlot = table.Column<int>(type: "int", nullable: false),
                    term = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_settingDB", x => x.version);
                });

            migrationBuilder.InsertData(
                table: "settingDB",
                columns: new[] { "version", "cntActionPerSlot", "maxSlot", "term" },
                values: new object[] { 1, 1, 10, 10 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "settingDB");
        }
    }
}
