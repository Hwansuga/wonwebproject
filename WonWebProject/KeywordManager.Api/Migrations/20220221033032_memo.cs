﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class memo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "bc111eb2-3525-4976-94db-100e965d067b");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "d141a17a-ad34-4ff4-b785-f73e014de7cb");

            migrationBuilder.AddColumn<string>(
                name: "memo1",
                table: "userPointDB",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "memo2",
                table: "userPointDB",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "9a594e31-4376-449e-9307-48fb018a992b", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "createTime", "goodsId", "owner", "sendTimeStamp", "tag" },
                values: new object[] { "0f460217-f840-4fe4-ac3d-120e956d53d4", 0, 0, new DateTime(2022, 2, 21, 12, 30, 31, 779, DateTimeKind.Local).AddTicks(8956), "", "", new DateTime(2022, 2, 21, 0, 0, 0, 0, DateTimeKind.Local), "" });

            migrationBuilder.UpdateData(
                table: "userPointDB",
                keyColumn: "id",
                keyValue: "test",
                columns: new[] { "expire", "memo1", "memo2" },
                values: new object[] { new DateTime(2022, 2, 21, 0, 0, 0, 0, DateTimeKind.Local), "", "" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "9a594e31-4376-449e-9307-48fb018a992b");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "0f460217-f840-4fe4-ac3d-120e956d53d4");

            migrationBuilder.DropColumn(
                name: "memo1",
                table: "userPointDB");

            migrationBuilder.DropColumn(
                name: "memo2",
                table: "userPointDB");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "bc111eb2-3525-4976-94db-100e965d067b", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "createTime", "goodsId", "owner", "sendTimeStamp", "tag" },
                values: new object[] { "d141a17a-ad34-4ff4-b785-f73e014de7cb", 0, 0, new DateTime(2022, 1, 5, 20, 14, 48, 807, DateTimeKind.Local).AddTicks(5022), "", "", new DateTime(2022, 1, 5, 0, 0, 0, 0, DateTimeKind.Local), "" });

            migrationBuilder.UpdateData(
                table: "userPointDB",
                keyColumn: "id",
                keyValue: "test",
                column: "expire",
                value: new DateTime(2022, 1, 5, 0, 0, 0, 0, DateTimeKind.Local));
        }
    }
}
