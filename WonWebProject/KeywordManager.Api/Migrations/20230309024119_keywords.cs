﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class keywords : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "f7db0983-7e57-42e0-bbf7-8e36b204ee0c");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "d0ecdf86-571a-4810-be1c-64ec4c292e3b");

            migrationBuilder.CreateTable(
                name: "CKDB",
                columns: table => new
                {
                    keywords = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    dateTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "searchKeyword", "state", "value1", "value2" },
                values: new object[] { "d58ade0d-7fc2-4637-ad0b-eda4203d1e5e", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "createTime", "goodsId", "owner", "sendTimeStamp", "tag" },
                values: new object[] { "a5626023-9c8d-41fa-84a8-707c116735c4", 0, 0, new DateTime(2023, 3, 9, 11, 41, 18, 423, DateTimeKind.Local).AddTicks(8588), "", "", new DateTime(2023, 3, 9, 0, 0, 0, 0, DateTimeKind.Local), "" });

            migrationBuilder.UpdateData(
                table: "userPointDB",
                keyColumn: "id",
                keyValue: "test",
                column: "expire",
                value: new DateTime(2023, 3, 9, 0, 0, 0, 0, DateTimeKind.Local));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CKDB");

            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "d58ade0d-7fc2-4637-ad0b-eda4203d1e5e");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "a5626023-9c8d-41fa-84a8-707c116735c4");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "searchKeyword", "state", "value1", "value2" },
                values: new object[] { "f7db0983-7e57-42e0-bbf7-8e36b204ee0c", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "createTime", "goodsId", "owner", "sendTimeStamp", "tag" },
                values: new object[] { "d0ecdf86-571a-4810-be1c-64ec4c292e3b", 0, 0, new DateTime(2022, 3, 21, 21, 10, 22, 816, DateTimeKind.Local).AddTicks(4598), "", "", new DateTime(2022, 3, 21, 0, 0, 0, 0, DateTimeKind.Local), "" });

            migrationBuilder.UpdateData(
                table: "userPointDB",
                keyColumn: "id",
                keyValue: "test",
                column: "expire",
                value: new DateTime(2022, 3, 21, 0, 0, 0, 0, DateTimeKind.Local));
        }
    }
}
