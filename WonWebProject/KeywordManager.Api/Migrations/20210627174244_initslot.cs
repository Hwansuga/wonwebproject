﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class initslot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "06265c8a-4182-4750-b75e-d4485bf9c631");

            migrationBuilder.CreateTable(
                name: "slotDB",
                columns: table => new
                {
                    id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    goodsId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    owner = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    create = table.Column<DateTime>(type: "datetime2", nullable: false),
                    expire = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_slotDB", x => x.id);
                });

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "28ade4d6-d1fe-4512-8352-f7c89fd4cfbb", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "create", "expire", "goodsId", "owner" },
                values: new object[] { "44c1ee3c-cb4e-4754-a9eb-c8c94ab87694", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1, 1, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "", "" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "slotDB");

            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "28ade4d6-d1fe-4512-8352-f7c89fd4cfbb");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "06265c8a-4182-4750-b75e-d4485bf9c631", "코인", "메모", "", "", "", "", "", "", "", "" });
        }
    }
}
