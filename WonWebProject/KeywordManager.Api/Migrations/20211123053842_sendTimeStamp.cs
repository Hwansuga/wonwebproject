﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class sendTimeStamp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "b46d4d16-64cd-4958-b999-eabf73cf9f45");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "0ac0ce0d-332a-4818-b0c3-3d5c73e3a089");

            migrationBuilder.DropColumn(
                name: "create",
                table: "slotDB");

            migrationBuilder.RenameColumn(
                name: "expire",
                table: "slotDB",
                newName: "sendTimeStamp");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "fef26708-40e6-4f24-b2ea-cf856ce90cf7", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "goodsId", "owner", "sendTimeStamp" },
                values: new object[] { "4f0be4a8-ab88-4522-a310-c5af8153991d", 0, 0, "", "", new DateTime(2021, 11, 23, 0, 0, 0, 0, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "userPointDB",
                keyColumn: "id",
                keyValue: "test",
                column: "expire",
                value: new DateTime(2021, 11, 23, 0, 0, 0, 0, DateTimeKind.Local));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "fef26708-40e6-4f24-b2ea-cf856ce90cf7");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "4f0be4a8-ab88-4522-a310-c5af8153991d");

            migrationBuilder.RenameColumn(
                name: "sendTimeStamp",
                table: "slotDB",
                newName: "expire");

            migrationBuilder.AddColumn<DateTime>(
                name: "create",
                table: "slotDB",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "b46d4d16-64cd-4958-b999-eabf73cf9f45", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "create", "expire", "goodsId", "owner" },
                values: new object[] { "0ac0ce0d-332a-4818-b0c3-3d5c73e3a089", 0, 0, new DateTime(2021, 11, 11, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2021, 11, 16, 0, 0, 0, 0, DateTimeKind.Local), "", "" });

            migrationBuilder.UpdateData(
                table: "userPointDB",
                keyColumn: "id",
                keyValue: "test",
                column: "expire",
                value: new DateTime(2021, 11, 11, 0, 0, 0, 0, DateTimeKind.Local));
        }
    }
}
