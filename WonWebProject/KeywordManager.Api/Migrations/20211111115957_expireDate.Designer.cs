﻿// <auto-generated />
using System;
using KeywordManager.Api.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace KeywordManager.Api.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20211111115957_expireDate")]
    partial class expireDate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.6")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("KeywordManager.Model.Goods", b =>
                {
                    b.Property<string>("id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("cntRecv")
                        .HasColumnType("int");

                    b.Property<int>("cntSend")
                        .HasColumnType("int");

                    b.Property<string>("customer")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("goodsName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("keyword")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("memo")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("option")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("owner")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("rank1")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("rank1Change")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("rank2")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("rank2Change")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("value1")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("value2")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("id");

                    b.ToTable("goodsDB");

                    b.HasData(
                        new
                        {
                            id = "b46d4d16-64cd-4958-b999-eabf73cf9f45",
                            cntRecv = 0,
                            cntSend = 0,
                            customer = "",
                            goodsName = "",
                            keyword = "코인",
                            memo = "메모",
                            option = "",
                            owner = "",
                            rank1 = "",
                            rank1Change = "",
                            rank2 = "",
                            rank2Change = "",
                            value1 = "",
                            value2 = ""
                        });
                });

            modelBuilder.Entity("KeywordManager.Model.Setting", b =>
                {
                    b.Property<int>("version")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("cntActionPerSlot")
                        .HasColumnType("int");

                    b.Property<int>("maxSlot")
                        .HasColumnType("int");

                    b.Property<int>("term")
                        .HasColumnType("int");

                    b.HasKey("version");

                    b.ToTable("settingDB");

                    b.HasData(
                        new
                        {
                            version = 1,
                            cntActionPerSlot = 1,
                            maxSlot = 10,
                            term = 10
                        });
                });

            modelBuilder.Entity("KeywordManager.Model.Slot", b =>
                {
                    b.Property<string>("id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("cntRecv")
                        .HasColumnType("int");

                    b.Property<int>("cntSend")
                        .HasColumnType("int");

                    b.Property<DateTime>("create")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("expire")
                        .HasColumnType("datetime2");

                    b.Property<string>("goodsId")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("owner")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("id");

                    b.ToTable("slotDB");

                    b.HasData(
                        new
                        {
                            id = "0ac0ce0d-332a-4818-b0c3-3d5c73e3a089",
                            cntRecv = 0,
                            cntSend = 0,
                            create = new DateTime(2021, 11, 11, 0, 0, 0, 0, DateTimeKind.Local),
                            expire = new DateTime(2021, 11, 16, 0, 0, 0, 0, DateTimeKind.Local),
                            goodsId = "",
                            owner = ""
                        });
                });

            modelBuilder.Entity("KeywordManager.Model.UserPoint", b =>
                {
                    b.Property<string>("id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("cntSlot")
                        .HasColumnType("int");

                    b.Property<DateTime>("expire")
                        .HasColumnType("datetime2");

                    b.Property<int>("point")
                        .HasColumnType("int");

                    b.HasKey("id");

                    b.ToTable("userPointDB");

                    b.HasData(
                        new
                        {
                            id = "test",
                            cntSlot = 1,
                            expire = new DateTime(2021, 11, 11, 0, 0, 0, 0, DateTimeKind.Local),
                            point = 10000
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
