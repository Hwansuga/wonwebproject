﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class InitGoods2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "1674968d-1932-4f93-960a-1897bc7394ec");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "06265c8a-4182-4750-b75e-d4485bf9c631", "코인", "메모", "", "", "", "", "", "", "", "" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "06265c8a-4182-4750-b75e-d4485bf9c631");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "1674968d-1932-4f93-960a-1897bc7394ec", "코인", "메모", null, null, "", "", "", "", null, null });
        }
    }
}
