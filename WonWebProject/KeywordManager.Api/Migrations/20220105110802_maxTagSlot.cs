﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Api.Migrations
{
    public partial class maxTagSlot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "e8ee3655-d0ac-4612-a59e-3120b4c75a0f");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "40671035-6df7-41d3-a1f7-70ea94b7a4c6");

            migrationBuilder.AddColumn<int>(
                name: "maxTagSlot",
                table: "settingDB",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "49fa0242-c57f-4333-9f83-9f0bdef19f63", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.UpdateData(
                table: "settingDB",
                keyColumn: "version",
                keyValue: 1,
                column: "maxTagSlot",
                value: 10);

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "createTime", "goodsId", "owner", "sendTimeStamp" },
                values: new object[] { "cc19b73d-da08-4d95-8896-8e71ea9b8678", 0, 0, new DateTime(2022, 1, 5, 20, 8, 1, 842, DateTimeKind.Local).AddTicks(6848), "", "", new DateTime(2022, 1, 5, 0, 0, 0, 0, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "userPointDB",
                keyColumn: "id",
                keyValue: "test",
                column: "expire",
                value: new DateTime(2022, 1, 5, 0, 0, 0, 0, DateTimeKind.Local));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "goodsDB",
                keyColumn: "id",
                keyValue: "49fa0242-c57f-4333-9f83-9f0bdef19f63");

            migrationBuilder.DeleteData(
                table: "slotDB",
                keyColumn: "id",
                keyValue: "cc19b73d-da08-4d95-8896-8e71ea9b8678");

            migrationBuilder.DropColumn(
                name: "maxTagSlot",
                table: "settingDB");

            migrationBuilder.InsertData(
                table: "goodsDB",
                columns: new[] { "id", "cntRecv", "cntSend", "customer", "goodsName", "keyword", "memo", "option", "owner", "rank1", "rank1Change", "rank2", "rank2Change", "value1", "value2" },
                values: new object[] { "e8ee3655-d0ac-4612-a59e-3120b4c75a0f", 0, 0, "", "", "코인", "메모", "", "", "", "", "", "", "", "" });

            migrationBuilder.InsertData(
                table: "slotDB",
                columns: new[] { "id", "cntRecv", "cntSend", "createTime", "goodsId", "owner", "sendTimeStamp" },
                values: new object[] { "40671035-6df7-41d3-a1f7-70ea94b7a4c6", 0, 0, new DateTime(2021, 12, 2, 20, 53, 34, 933, DateTimeKind.Local).AddTicks(9999), "", "", new DateTime(2021, 12, 2, 0, 0, 0, 0, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "userPointDB",
                keyColumn: "id",
                keyValue: "test",
                column: "expire",
                value: new DateTime(2021, 12, 2, 0, 0, 0, 0, DateTimeKind.Local));
        }
    }
}
