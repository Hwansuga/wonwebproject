﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Api
{
    public class Util
    {
        public static void Chunks<T>(List<T> _source , int _cnt , ref List<List<T>> ret)
        {
            for (int i = 0; i < _source.Count; i += _cnt)
            {
                ret.Add(_source.GetRange(i, Math.Min(_cnt, _source.Count - i)));
            }
        }
    }
}
