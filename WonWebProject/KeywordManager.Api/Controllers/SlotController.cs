﻿using KeywordManager.Api.Model;
using KeywordManager.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace KeywordManager.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SlotController : ControllerBase
    {
        readonly ISlotRepository slotRepository;
        public SlotController(ISlotRepository slotRepository)
        {
            this.slotRepository = slotRepository;
        }

        // GET: api/<ValuesController>
        [HttpGet]
        public async Task<ActionResult> GetInfos()
        {
            try
            {
                return Ok(await slotRepository.GetInfos());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        [HttpGet("search")]
        public async Task<ActionResult<IEnumerable<Slot>>> Search(string owner)
        {
            try
            {
                var result = await slotRepository.Search(owner);

                return Ok(result);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Slot>> GetInfo(string id)
        {
            try
            {
                var result = await slotRepository.GetInfo(id);
                if (result == null)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
                }

                return result;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        // POST api/<ValuesController>
        [HttpPost]
        public async Task<ActionResult<Slot>> AddInfo(Slot value)
        {
            try
            {
                if (value == null)
                    return BadRequest();

                var info = await slotRepository.GetInfo(value.id);
                if (info != null)
                {
                    ModelState.AddModelError("id", "exist value");
                    return BadRequest(ModelState);
                }

                var addInfo = await slotRepository.AddInfo(value);

                return CreatedAtAction(nameof(GetInfo), new { id = Guid.NewGuid().ToString("N") }, value);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        [HttpPost("AddSlot/{_owner}/{_cnt}")]
        public async Task<ActionResult<IEnumerable<Slot>>> AddSlot(string _owner, int _cnt)
        {
            try
            {
                var result = await slotRepository.AddSlot(_owner , _cnt);
                return Ok(result);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Failed to add empty slot!");
            }
        }

        // PUT api/<ValuesController>/5
        [HttpPut]
        public async Task<ActionResult<Slot>> UpdateInfo(Slot value)
        {
            try
            {
                var infoToUpdate = await slotRepository.GetInfo(value.id);
                if (infoToUpdate == null)
                {
                    return NotFound($"{value.GetType()} with id = {value.id} not found");
                }

                return await slotRepository.UpdateInfo(value);
            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        [HttpPut("UpdateSlots")]
        public async Task<ActionResult<IEnumerable<Slot>>> UpdateInfos(Slot[] value)
        {
            try
            {
                return Ok(await slotRepository.UpdateInfos(value));
            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Slot>> Delete(string id)
        {
            try
            {
                var infoToDelete = await slotRepository.GetInfo(id);
                if (infoToDelete == null)
                {
                    return NotFound($"Setting with id = {id} not found");
                }

                return await slotRepository.DeleteInfo(id);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        [HttpDelete("DeleteByOwner")]
        public async Task<ActionResult> DeleteByOwner(string owner)
        {
            try
            {
                await slotRepository.DeleteInfoByOwner(owner);

                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        [HttpGet("SearchGoods")]
        public async Task<ActionResult<Slot>> GetInfoByGoodsId(string goodsId)
        {
            try
            {
                var result = await slotRepository.GetInfoByGoodsId(goodsId);
                if (result == null)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
                }

                return result;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        [HttpPut("ClearTag")]

        public async Task<ActionResult> ClearTag(string _tag)
        {
            try
            {
                await slotRepository.ClearTag(_tag);
                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"failed to clear tag : {_tag}");
            }
        }

        [HttpPut("UpdateCntWork")]
        public async Task<ActionResult<Slot>> UpdateCntWork(string _id)
        {
            try
            {
                return Ok(await slotRepository.UpdateCntWork(_id));
            }
            catch
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }
    }
}
