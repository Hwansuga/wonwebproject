﻿using KeywordManager.Api.Model;
using KeywordManager.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserPointController : ControllerBase
    {
        
        readonly IUserPointRepository userPointRepository;

        public UserPointController(IUserPointRepository userPointRepository)
        {
            this.userPointRepository = userPointRepository;
        }

        [HttpGet]
        public async Task<ActionResult> GetUserInfos()
        {
            try
            {
                return Ok(await userPointRepository.GetInfos());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<UserPoint>> GetInfo(string id)
        {
            try
            {
                var result = await userPointRepository.GetInfo(id);
                if (result  == null)
                {
                    var newUser = new UserPoint();
                    newUser.id = id;
                    newUser.point = 0;

                    return AddUserPoint(newUser).Result;
                }

                return result;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }
        
        [HttpPost]
        public async Task<ActionResult<UserPoint>> AddUserPoint(UserPoint userPoint)
        {
            try
            {
                if (userPoint == null)
                    return BadRequest(); 

                var user = await userPointRepository.GetInfo(userPoint.id);
                if (user != null)
                {
                    ModelState.AddModelError("id", "exist user");
                    return BadRequest(ModelState);
                }

                var addUser = await userPointRepository.AddUserPoint(userPoint);

                return CreatedAtAction(nameof(GetInfo), new { id = addUser.id }, addUser ) ;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }
        
        [HttpPut]
        public async Task<ActionResult<UserPoint>> UpdateUserPoint(UserPoint userPoint)
        {
            try
            {
                var userToUpdate = await userPointRepository.GetInfo(userPoint.id);
                if (userToUpdate == null)
                {
                    return NotFound($"UserPoint with id = {userPoint.id} not found" );
                }

                return await userPointRepository.UpdateUserPoint(userPoint);
            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<UserPoint>> DeleteUserPoint(string id)
        {
            try
            {
                var userToDelete = await userPointRepository.GetInfo(id);
                if (userToDelete == null)
                {
                    return NotFound($"UserPoint with id = {id} not found");
                }

                return await userPointRepository.DeleteUserPoint(id);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }
        
    }
}
