﻿using KeywordManager.Api.Model;
using KeywordManager.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace KeywordManager.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GoodsController : ControllerBase
    {
        readonly IGoodsRepository goodsRepository;

        public GoodsController(IGoodsRepository goodsRepository)
        {
            this.goodsRepository = goodsRepository;
        }

        // GET: api/<GoodsController>
        [HttpGet]
        public async Task<ActionResult> GetInfos()
        {
            try
            {
                return Ok(await goodsRepository.GetInfos());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        [HttpGet("search")]
        public async Task<ActionResult<IEnumerable<Goods>>> Search(string owner)
        {
            try
            {
                var result = await goodsRepository.Search(owner);

                return Ok(result);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        [HttpGet("SearchByKeyWord")]
        public async Task<ActionResult<IEnumerable<Goods>>> SearchByKeyWord(string keyword)
        {
            try
            {
                var result = await goodsRepository.SearchByKeyWord(keyword);

                return Ok(result);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        // GET api/<GoodsController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Goods>> GetInfo(string id)
        {
            try
            {
                var result = await goodsRepository.GetInfo(id);
                if (result == null)
                {
                    return new Goods();
                }

                return result;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        // POST api/<GoodsController>
        [HttpPost]
        public async Task<ActionResult<Goods>> AddInfo(Goods value)
        {
            try
            {
                return Ok(await goodsRepository.AddInfo(value));
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        [HttpPost("AddInfos")]
        public async Task<ActionResult<Goods>> AddInfos(List<Goods> values)
        {
            try
            {
                return Ok(await goodsRepository.AddInfos(values));
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        // PUT api/<GoodsController>/5
        [HttpPut]
        public async Task<ActionResult<Goods>> UpdateInfo(Goods value)
        {
            try
            {
                var infoToUpdate = await goodsRepository.GetInfo(value.id);
                if (infoToUpdate == null)
                {
                    return NotFound($"{value.GetType()} with id = {value.id} not found");
                }

                return await goodsRepository.UpdateInfo(value);
            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        // DELETE api/<GoodsController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Goods>> Delete(string id)
        {
            try
            {
                var infoToDelete = await goodsRepository.GetInfo(id);
                if (infoToDelete == null)
                {
                    return NotFound($"Setting with id = {id} not found");
                }

                return await goodsRepository.DeleteInfo(id);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        [HttpDelete("DeleteByOwner")]
        public async Task<ActionResult> DeleteByOwner(string owner)
        {
            try
            {
                await goodsRepository.DeleteInfoByOwner(owner);

                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        [HttpPost("GetLowWokingItem")]
        public async Task<ActionResult<IEnumerable<Goods>>> GetLowWokingItem(int cnt)
        {
            try
            {
                var result = await goodsRepository.GetLowWokingItem(cnt);
                return Ok(result);
            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        [HttpPost("GetItemsByTag")]
        public async Task<ActionResult<IEnumerable<Goods>>> GetItemsByTag(string _tag)
        {
            try
            {
                var result = await goodsRepository.GetItemsByTag(_tag);
                return Ok(result);
            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        [HttpPost("GetAllItems")]
        public async Task<ActionResult<IEnumerable<Goods>>> GetAllItems()
        {
            try
            {
                var result = await goodsRepository.GetAllItems();
                return Ok(result);
            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        [HttpPut("UpdateRankInfo")]
        public async Task<ActionResult<Goods>> UpdateRankInfo(Goods value)
        {
            try
            {
                var infoToUpdate = await goodsRepository.GetInfo(value.id);
                if (infoToUpdate == null)
                {
                    return NotFound($"{value.GetType()} with id = {value.id} not found");
                }

                return await goodsRepository.UpdateRankInfo(value); 
            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }
    }
}
