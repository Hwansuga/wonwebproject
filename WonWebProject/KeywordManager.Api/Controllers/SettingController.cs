﻿using KeywordManager.Api.Model;
using KeywordManager.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace KeywordManager.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SettingController : ControllerBase
    {
        readonly ISettingRepository settingRepository;

        public SettingController(ISettingRepository settingRepository)
        {
            this.settingRepository = settingRepository;
        }
        // GET: api/<ValuesController>
        [HttpGet]
        public async Task<ActionResult> GetInfos()
        {
            try
            {
                return Ok(await settingRepository.GetInfos());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Setting>> GetInfo(int id)
        {
            try
            {
                var result = await settingRepository.GetInfo(id);
                if (result == null)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
                }

                return result;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        // POST api/<ValuesController>
        [HttpPost]
        public async Task<ActionResult<Setting>> AddInfo(Setting setting)
        {
            try
            {
                if (setting == null)
                    return BadRequest();

                var user = await settingRepository.GetInfo(setting.version);
                if (user != null)
                {
                    ModelState.AddModelError("id", "exist version");
                    return BadRequest(ModelState);
                }

                var addSetting = await settingRepository.AddInfo(setting);
                var listSetting = (await settingRepository.GetInfos()).ToList();

                return CreatedAtAction(nameof(GetInfo), new { version = listSetting.Count +1 }, addSetting);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        // PUT api/<ValuesController>/5
        [HttpPut]
        public async Task<ActionResult<Setting>> UpdateInfo(Setting setting)
        {
            try
            {
                var infoToUpdate = await settingRepository.GetInfo(setting.version);
                if (infoToUpdate == null)
                {
                    return NotFound($"{setting.GetType()} with id = {setting.version} not found");
                }

                return await settingRepository.UpdateInfo(setting);
            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Setting>> DeleteInfo(int version)
        {
            try
            {
                var infoToDelete = await settingRepository.GetInfo(version);
                if (infoToDelete == null)
                {
                    return NotFound($"Setting with id = {version} not found");
                }

                return await settingRepository.DeleteInfo(version);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }
    }
}
