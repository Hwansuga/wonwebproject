﻿using Microsoft.EntityFrameworkCore;
using KeywordManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Api.Model
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {

        }

        public DbSet<UserPoint> userPointDB { get; set; }

        public DbSet<Setting> settingDB { get; set; }

        public DbSet<Goods> goodsDB { get; set; }

        public DbSet<Slot> slotDB { get; set; }

        public DbSet<CombinationKeyword> CKDB {get;set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UserPoint>().HasData(new UserPoint { 
                id = "test",
                point = 10000
            });

            modelBuilder.Entity<Setting>().HasData(new Setting());

            modelBuilder.Entity<Goods>().HasData(new Goods
            {
                id = Guid.NewGuid().ToString(),
                keyword = "코인",
                memo = "메모",
            });

            modelBuilder.Entity<Slot>().HasData(new Slot {
                id = Guid.NewGuid().ToString(),
            });
        }
    }
}
