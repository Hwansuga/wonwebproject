﻿using System.Threading.Tasks;

namespace KeywordManager.Api.Model
{
    public interface IBackgroundWorker
    {
        Task ResetCntSend();

        Task CheckExpireAcc();

        Task ResetCKDB();
    }
}