﻿using KeywordManager.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Api.Model
{
    public class UserPointRepository : IUserPointRepository
    {

        readonly AppDbContext appDbContext;

        public UserPointRepository(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }
        public async Task<UserPoint> AddUserPoint(UserPoint userPoint)
        {
            var result = await appDbContext.userPointDB.AddAsync(userPoint);
            await appDbContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<UserPoint> DeleteUserPoint(string id)
        {
            var result = await appDbContext.userPointDB.FirstOrDefaultAsync(e => e.id == id);
            if (result != null)
            {
                appDbContext.userPointDB.Remove(result);
                await appDbContext.SaveChangesAsync();

                return result;
            }

            return null;

        }

        public async Task<UserPoint> GetInfo(string id)
        {
            return await appDbContext.userPointDB.FirstOrDefaultAsync(e => e.id ==  id);
        }

        public async Task<IEnumerable<UserPoint>> GetInfos()
        {
            return await appDbContext.userPointDB.ToListAsync();
        }

        public async Task<UserPoint> UpdateUserPoint(UserPoint userPoint)
        {
            
            var result = await appDbContext.userPointDB.FirstOrDefaultAsync(e => e.id == userPoint.id);
            if (result != null)
            {
                //appDbContext.Entry(userPoint).State = EntityState.Modified;
                appDbContext.Entry(result).CurrentValues.SetValues(userPoint);
                await appDbContext.SaveChangesAsync();

                return result;
            }

            return null;
        }
    }
}
