﻿using KeywordManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Api.Model
{
    public interface ISettingRepository
    {
        Task<IEnumerable<Setting>> GetInfos();
        Task<Setting> GetInfo(int version);
        Task<Setting> AddInfo(Setting setting);
        Task<Setting> UpdateInfo(Setting setting);
        Task<Setting> DeleteInfo(int version);
    }
}
