﻿using KeywordManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Api.Model
{
    public interface IGoodsRepository
    {
        Task<IEnumerable<Goods>> GetInfos();
        Task<IEnumerable<Goods>> SearchByKeyWord(string keyword_);
        Task<Goods> GetInfo(string id);
        
        Task<Goods> AddInfo(Goods goods);
        Task<IEnumerable<Goods>> AddInfos(List<Goods> values);
        Task<Goods> UpdateInfo(Goods goods);
        Task<Goods> DeleteInfo(string id);

        Task DeleteInfoByOwner(string owner);

        Task<IEnumerable<Goods>> Search(string owner);

        Task<IEnumerable<Goods>> GetLowWokingItem(int cnt);

        Task<IEnumerable<Goods>> GetItemsByTag(string _tag);
        Task<IEnumerable<Goods>> GetAllItems();

        Task<Goods> UpdateRankInfo(Goods goods);
    }
}
