﻿using KeywordManager.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Api.Model
{
    public class SettingRepository : ISettingRepository
    {
        readonly AppDbContext appDbContext;
        public SettingRepository(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }
        public async Task<Setting> AddInfo(Setting setting)
        {
            var result = await appDbContext.settingDB.AddAsync(setting);
            await appDbContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<Setting> DeleteInfo(int version)
        {
            var result = await appDbContext.settingDB.FirstOrDefaultAsync(e => e.version == version);
            if (result != null)
            {
                appDbContext.settingDB.Remove(result);
                await appDbContext.SaveChangesAsync();

                return result;
            }

            return null;
        }

        public async Task<Setting> GetInfo(int version)
        {
            return await appDbContext.settingDB.FirstOrDefaultAsync(e => e.version == version);
        }

        public async Task<IEnumerable<Setting>> GetInfos()
        {
            return await appDbContext.settingDB.ToListAsync();
        }

        public async Task<Setting> UpdateInfo(Setting setting)
        {
            var result = await appDbContext.settingDB.FirstOrDefaultAsync(e => e.version == setting.version);
            if (result != null)
            {
                result.cntActionPerSlot = setting.cntActionPerSlot;
                result.maxSlot = setting.maxSlot;
                result.term = setting.term;
                result.maxTagSlot = setting.maxTagSlot;
                await appDbContext.SaveChangesAsync();

                return result;
            }

            return null;
        }
    }
}
