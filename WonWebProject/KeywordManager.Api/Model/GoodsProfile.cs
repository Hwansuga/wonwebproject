﻿using AutoMapper;
using KeywordManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Api.Model
{
    public class GoodsProfile : Profile
    {
        public GoodsProfile()
        {
            CreateMap<Goods, Goods>();
        }
    }
}
