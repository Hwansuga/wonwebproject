﻿using KeywordManager.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Api.Model
{
    public class SlotRepository : ISlotRepository
    {
        readonly AppDbContext appDbContext;
        public SlotRepository(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }
        public async Task<Slot> AddInfo(Slot value)
        {
            var result = await appDbContext.slotDB.AddAsync(value);
            await appDbContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<IEnumerable<Slot>> AddSlot(string _owner, int _cnt)
        {
            List<Slot> ret = new List<Slot>();
            for(int i=0; i<_cnt; ++i)
            {
                Slot temp = new Slot();
                temp.id = Guid.NewGuid().ToString();
                temp.owner = _owner;
                var add = await appDbContext.slotDB.AddAsync(temp);
                ret.Add(add.Entity);
            }

            await appDbContext.SaveChangesAsync();

            return ret;
        }

        public async Task<Slot> DeleteInfo(string id)
        {
            var result = await appDbContext.slotDB.FirstOrDefaultAsync(e => e.id == id);
            if (result != null)
            {
                appDbContext.slotDB.Remove(result);
                await appDbContext.SaveChangesAsync();

                return result;
            }

            return null;
        }

        public async Task DeleteInfoByOwner(string owner)
        {
            var listSlot = await appDbContext.slotDB.Where(e => e.owner == owner).ToListAsync();
            if (listSlot.Count > 0)
            {
                appDbContext.slotDB.RemoveRange(listSlot.ToArray());
                await appDbContext.SaveChangesAsync();
            }
        }

        public async Task<Slot> GetInfo(string id)
        {
            return await appDbContext.slotDB.FirstOrDefaultAsync(e => e.id == id);
        }

        public async Task<IEnumerable<Slot>> GetInfos()
        {
            return await appDbContext.slotDB.ToListAsync();
        }

        public async Task<IEnumerable<Slot>> Search(string owner)
        {
            return await appDbContext.slotDB.Where(e => e.owner == owner).ToListAsync();
        }

        public async Task<Slot> UpdateInfo(Slot value)
        {
            var result = await appDbContext.slotDB.FirstOrDefaultAsync(e => e.id == value.id);
            if (result != null)
            {
                appDbContext.Entry(result).CurrentValues.SetValues(value);
                await appDbContext.SaveChangesAsync();

                return result;
            }

            return null;
        }

        public async Task<IEnumerable<Slot>> UpdateInfos(Slot[] value)
        {
            var listUpdate = new List<Slot>();
            foreach(var slot in value)
            {
                var result = await appDbContext.slotDB.FirstOrDefaultAsync(e => e.id == slot.id);
                if (result != null)
                {
                    appDbContext.Entry(result).CurrentValues.SetValues(slot);
                    listUpdate.Add(slot);
                }                
            }

            await appDbContext.SaveChangesAsync();

            return listUpdate;
        }

        public async Task<Slot> GetInfoByGoodsId(string goodsId_)
        {
            return await appDbContext.slotDB.FirstOrDefaultAsync(e => e.goodsId == goodsId_);
        }

        public async Task ClearTag(string _tag)
        {
            if (string.IsNullOrEmpty(_tag))
            {
                await appDbContext.slotDB.ForEachAsync(e => e.tag = string.Empty);
            }
            else
            {
                var listTarget = await appDbContext.slotDB.Where(e => e.tag == _tag).ToListAsync();
                listTarget.ForEach(x => x.tag = string.Empty);
            }
            await appDbContext.SaveChangesAsync();
        }

        public async Task<Slot> UpdateCntWork(string _goodsId)
        {
            var slot = await appDbContext.slotDB.FirstOrDefaultAsync(e => e.goodsId == _goodsId);
            if (slot != null)
            {
                slot.cntSend += 1;
                await appDbContext.SaveChangesAsync();
            }

            return slot;
        }
    }
}
