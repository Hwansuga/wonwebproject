﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Api.Model
{
    public class BackgroundWorker : IBackgroundWorker
    {
        private readonly AppDbContext appDbContext;

        public BackgroundWorker(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public async Task ResetCntSend()
        {
            await appDbContext.slotDB.ForEachAsync(x => x.cntSend = 0);

            var listExpireUp = await appDbContext.userPointDB.Where(x => x.expire.Date < DateTime.Now.Date).ToListAsync();
            var listSlot = await appDbContext.slotDB.ToListAsync();
            listSlot = listSlot.FindAll(s => listExpireUp.Find(u => u.id == s.owner) != null);
            listSlot.ForEach(x => x.tag = string.Empty);


            await appDbContext.SaveChangesAsync();
        }

        public async Task CheckExpireAcc()
        {
            var listExpireUp = await appDbContext.userPointDB.Where(x => x.expire.Date < DateTime.Now.Date).ToListAsync();
            var listSlot = await appDbContext.slotDB.ToListAsync();
            listSlot = listSlot.FindAll(s => listExpireUp.Find(u => u.id == s.owner) != null);
            listSlot.ForEach(x => x.tag = string.Empty);
            await appDbContext.SaveChangesAsync();
        }

        public async Task ResetCKDB()
        {
            appDbContext.CKDB.RemoveRange(appDbContext.CKDB);
            await appDbContext.SaveChangesAsync();
        }
    }
}
