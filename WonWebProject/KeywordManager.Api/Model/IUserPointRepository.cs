﻿using KeywordManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Api.Model
{
    public interface IUserPointRepository
    {
        Task<IEnumerable<UserPoint>> GetInfos();
        Task<UserPoint> GetInfo(string id);
        Task<UserPoint> AddUserPoint(UserPoint userPoint);
        Task<UserPoint> UpdateUserPoint(UserPoint userPoint);
        Task<UserPoint> DeleteUserPoint(string id);
    }
}
