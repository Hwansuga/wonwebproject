﻿using KeywordManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Api.Model
{
    public interface ISlotRepository
    {
        Task<IEnumerable<Slot>> GetInfos();
        Task<Slot> GetInfo(string id);
        Task<Slot> AddInfo(Slot value);

        Task<IEnumerable<Slot>> AddSlot(string _owner, int _cnt);

        Task<Slot> UpdateInfo(Slot value);

        Task<IEnumerable<Slot>> UpdateInfos(Slot[] value);
        Task<Slot> DeleteInfo(string id);

        Task DeleteInfoByOwner(string owner);
        Task<IEnumerable<Slot>> Search(string owner);

        Task<Slot> GetInfoByGoodsId(string goodsId_);

        Task ClearTag(string _tag);

        Task<Slot> UpdateCntWork(string _goodsId);
    }
}
