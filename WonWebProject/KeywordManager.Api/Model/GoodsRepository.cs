﻿using AutoMapper;
using KeywordManager.Model;
using Microsoft.AspNetCore.Components;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Api.Model
{
    public class GoodsRepository : IGoodsRepository
    {
        readonly AppDbContext appDbContext;
        public GoodsRepository(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public async Task<Goods> AddInfo(Goods goods)
        {
            var result = await appDbContext.goodsDB.AddAsync(goods);
            await appDbContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<IEnumerable<Goods>> AddInfos(List<Goods> values)
        {
            await appDbContext.goodsDB.AddRangeAsync(values);
            await appDbContext.SaveChangesAsync();
            return values;
        }

        public async Task<Goods> DeleteInfo(string id)
        {
            var result = await appDbContext.goodsDB.FirstOrDefaultAsync(e => e.id == id);
            if (result != null)
            {
                appDbContext.goodsDB.Remove(result);
                await appDbContext.SaveChangesAsync();

                return result;
            }

            return null;
        }

        public async Task DeleteInfoByOwner(string owner)
        {
            var listItem = await appDbContext.goodsDB.Where(e => e.owner == owner).ToListAsync();
            if (listItem.Count > 0)
            {
                appDbContext.goodsDB.RemoveRange(listItem.ToArray());
                await appDbContext.SaveChangesAsync();
            }
        }

        public async Task<Goods> GetInfo(string id)
        {
            return await appDbContext.goodsDB.FirstOrDefaultAsync(e => e.id == id);
        }

        public async Task<IEnumerable<Goods>> GetInfos()
        {
            return await appDbContext.goodsDB.ToListAsync();
        }

        public async Task<IEnumerable<Goods>> SearchByKeyWord(string keyword_)
        {
            return await appDbContext.goodsDB.Where(x=>x.keyword == keyword_).ToListAsync();
        }

        public async Task<Goods> UpdateInfo(Goods goods)
        {
            var result = await appDbContext.goodsDB.FirstOrDefaultAsync(e => e.id == goods.id);
            if (result != null)
            {
                
                //result = (Goods)goods.Clone();
                appDbContext.Entry(result).CurrentValues.SetValues(goods);
                await appDbContext.SaveChangesAsync();

                return result;
            }

            return null;
        }

        public async Task<IEnumerable<Goods>> Search(string owner)
        {
            return await appDbContext.goodsDB.Where(e => e.owner == owner).ToListAsync();

        }

        async Task<bool> CheckDuplicateCombination(List<Goods> _listGoods)
        {
            if (_listGoods.Count <= 0)
                return true;

            var listTargetKeyword = _listGoods.Select(x => x.keyword).ToList();
            
            try
            {
                var combiantionInfo = await appDbContext.CKDB.Where(x => x.dateTime.Date == DateTime.Today).ToListAsync();
                var sameLenghtElems = combiantionInfo.FindAll(x => x.keywords.Split(';').Length == listTargetKeyword.Count).ToList();
                foreach (var item in sameLenghtElems)
                {
                    var listKeyword = item.keywords.Split(';').ToList();
                    var unique = listKeyword.Except(listTargetKeyword).ToList();
                    if (unique.Count <= 0)
                        return false;
                }

                var ret = await appDbContext.CKDB.AddAsync(
                    new CombinationKeyword { 
                        id = Guid.NewGuid().ToString(), 
                        keywords = string.Join(';', listTargetKeyword), 
                        dateTime = DateTime.Now 
                    });
            }
            catch(Exception ex)
            {

            }
            
            return true;
        }

        public async Task<IEnumerable<Goods>> GetLowWokingItem(int cnt)
        {
            var setting = await appDbContext.settingDB.FirstOrDefaultAsync(e => e.version == 1);
            var listUserPoint = await appDbContext.userPointDB.Where(x => x.expire.Date >= DateTime.Now.Date).ToListAsync();

            var listSlot = await appDbContext.slotDB.ToListAsync();
            listSlot = listSlot.FindAll(x => x.cntSend < setting.cntActionPerSlot);
            listSlot = listSlot.FindAll(s => listUserPoint.Find(u => u.id == s.owner) != null);
            listSlot = listSlot.FindAll(x => x.sendTimeStamp.AddMinutes(setting.term) < DateTime.Now);

            listSlot = listSlot.OrderBy(x => x.cntRecv).ThenBy(x => x.cntSend).ToList();
            
            List<KeyValuePair<Slot, Goods>> sortSlot = new List<KeyValuePair<Slot, Goods>>();
            List<Slot> exceptList = new List<Slot>();
            while (true)
            {
                Dictionary<Slot, Goods> dicSlot = new Dictionary<Slot, Goods>();
                listSlot = listSlot.Except(exceptList).ToList();
                foreach (var itemSlot in listSlot)
                {
                    var goods = await appDbContext.goodsDB.FirstOrDefaultAsync(e => e.id == itemSlot.goodsId);
                    if (goods != null)
                    {
                        var listGoods = dicSlot.Values.ToList();

                        //check duplication
                        var checkGoods = listGoods.Find(x =>
                                        //x.keyword == goods.keyword && 
                                        x.value1 == goods.value1 &&
                                        x.value2 == goods.value2);
                        if (checkGoods == null)
                        {
                            dicSlot.Add(itemSlot, goods);
                        }
                    }
                }

                sortSlot = dicSlot.ToList();
                if (sortSlot.Count > cnt)
                {
                    List<KeyValuePair<Slot, Goods>> temp = new List<KeyValuePair<Slot, Goods>>();
                    foreach (var item in sortSlot)
                    {
                        if (temp.Count >= cnt)
                            break;

                        var checkExist = temp.Any(x => x.Value.keyword == item.Value.keyword);
                        if (checkExist)
                            continue;

                        temp.Add(item);
                    }
                    sortSlot = temp;

                }

                if (await CheckDuplicateCombination(sortSlot.Select(x => x.Value).ToList()))
                {                  
                    break;
                }
                else
                {
                    exceptList.Add(sortSlot[0].Key);                       
                }
                    
            }
                                      
            sortSlot.ForEach( (x) => { x.Key.cntSend += 1; x.Key.sendTimeStamp = DateTime.Now; });
            await appDbContext.SaveChangesAsync();

            return sortSlot.Select(x=>x.Value);
        }

        public async Task<IEnumerable<Goods>> GetItemsByTag(string _tag)
        {
            var setting = await appDbContext.settingDB.FirstOrDefaultAsync(e => e.version == 1);
            var listUserPoint = await appDbContext.userPointDB.Where(x => x.expire.Date >= DateTime.Now.Date).ToListAsync();
            var listSlot = await appDbContext.slotDB.ToListAsync();
            listSlot = listSlot.FindAll(s => listUserPoint.Find(u => u.id == s.owner) != null);
            listSlot = listSlot.FindAll(x => string.IsNullOrEmpty(x.goodsId) == false);

            var listTag = listSlot.FindAll(x => x.tag == _tag);
            if (listTag.Count > setting.maxTagSlot)
            {
                var retChunks = new List<List<Slot>>();
                Util.Chunks<Slot>(listTag , setting.maxTagSlot , ref retChunks);

                listTag = retChunks[0];
                for (int i = 1; i < retChunks.Count; ++i)
                    retChunks[i].ForEach(x => x.tag = string.Empty);

                await appDbContext.SaveChangesAsync();
            }

            var listGoods = await appDbContext.goodsDB.ToListAsync();
            var listTagGoods = listGoods.FindAll(x => listTag.Find(y => y.goodsId == x.id) != null);

            if (listTag.Count < setting.maxTagSlot)
            {
                var cntNeed = setting.maxTagSlot - listTag.Count;

                var listNoTag = listSlot.FindAll(x => string.IsNullOrEmpty(x.tag));
                var listNoTagGoods = listGoods.FindAll(x => listNoTag.Find(s => s.goodsId == x.id) != null);
                var sortGoods = listNoTagGoods.GroupBy(x => x.keyword).OrderByDescending(x => x.Count()).Select(g => g.First()).ToList();
                //suffle
//                 var rnd = new Random();
//                 listNoTag = listNoTag.OrderBy(item => rnd.Next()).ToList();

                int cnt = 0;
                foreach(var goods in sortGoods)
                {
                    if (cnt >= cntNeed)
                        break;

                    var checkGoods = listTagGoods.Find(x =>
                                    //x.keyword == goods.keyword && 
                                    (x.value1 == goods.value1 && x.value2 == goods.value2)
                                    || x.keyword == goods.keyword);

                    if (checkGoods == null)
                    {
                        cnt++;

                        var slot = listNoTag.Find(x => x.goodsId == goods.id);
                        slot.tag = _tag;
                        listTag.Add(slot);
                        listTagGoods.Add(goods);
                    }
                }

                await appDbContext.SaveChangesAsync();
            }

            listTagGoods = listTagGoods.FindAll(x => listTag.Find(u => u.goodsId == x.id && u.cntSend < setting.cntActionPerSlot) != null);

            return listTagGoods;
        }

        public async Task<IEnumerable<Goods>> GetAllItems()
        {
            var listUserPoint = await appDbContext.userPointDB.Where(x => x.expire.Date >= DateTime.Now.Date).ToListAsync();
            var listSlot = await appDbContext.slotDB.ToListAsync();
            listSlot = listSlot.FindAll(s => listUserPoint.Find(u => u.id == s.owner) != null);
            listSlot = listSlot.FindAll(x => string.IsNullOrEmpty(x.goodsId) == false);

            var listGoods = await appDbContext.goodsDB.ToListAsync();
            listGoods = listGoods.FindAll(x => listSlot.Find(y => y.goodsId == x.id) != null);

            return listGoods;
        }

        public async Task<Goods> UpdateRankInfo(Goods goods)
        {
            var result = await appDbContext.goodsDB.FirstOrDefaultAsync(e => e.id == goods.id);
            if (result != null)
            {
                result.rank1 = goods.rank1;
                result.rank2 = goods.rank2;
                result.rank1Change = goods.rank1Change;
                result.rank2Change = goods.rank2Change;
                result.state = goods.state;
                result.cntRecv += 1;

//                 var slot = await appDbContext.slotDB.FirstOrDefaultAsync(e => e.goodsId == goods.id);
//                 if (slot != null)
//                 {
//                     slot.cntSend += 1;
//                 }

                await appDbContext.SaveChangesAsync();

                return result;
            }

            return null;
        }
    }
}
