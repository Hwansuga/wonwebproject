﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Api
{
    public interface IJwtAuthenticationManager
    {
        string Authenticate(string seed);
    }
}
