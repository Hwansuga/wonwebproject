﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeywordManager.Model
{
    public class Setting
    {
        [Key]
        public int version { get; set; } = 1;
        public int cntActionPerSlot { get; set; } = 1;
        public int maxSlot { get; set; } = 10;
        public int term { get; set; } = 10;
        public int maxTagSlot { get; set; } = 10;
    }
}
