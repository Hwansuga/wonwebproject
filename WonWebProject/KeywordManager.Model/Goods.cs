﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeywordManager.Model
{
    public class Goods : ICloneable
    {
        [Key]
        public string id { get; set; } = "";

        public string searchKeyword { get; set; } = "";
        public string keyword { get; set; } = "";
        public string value1 { get; set; } = "";
        public string value2 { get; set; } = "";
        public string option { get; set; } = "";
        public string rank1 { get; set; } = "";
        public string rank1Change { get; set; } = "";
        public string rank2 { get; set; } = "";
        public string rank2Change { get; set; } = "";
        public string goodsName { get; set; } = "";
        public string customer { get; set; } = "";
        public string memo { get; set; } = "";
        public string state { get; set; } = "";
        public string owner { get; set; } = "";

        public int cntSend { get; set; } = 0;
        public int cntRecv { get; set; } = 0;
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
