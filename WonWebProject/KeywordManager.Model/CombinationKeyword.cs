﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeywordManager.Model
{

    public class CombinationKeyword
    {
        [Key]
        public string id { get; set; } = string.Empty;
        public string keywords { get; set; } = string.Empty;
        public DateTime dateTime { get; set; } = DateTime.Now;
    }
}
