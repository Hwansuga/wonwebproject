﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeywordManager.Model
{
    public class UserPoint
    {
        [Required(ErrorMessage = "uid must be provided")]
        public string id { get; set; }
        public int point { get; set; } = 0;
        public int cntSlot { get; set; } = 1;

        public string memo1 { get; set; } = string.Empty;
        public string memo2 { get; set; } = string.Empty;

        public DateTime expire { get; set; } = DateTime.Today;
    }
}
