﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeywordManager.Model
{
    public class Slot
    {
        [Key]
        public string id { get; set; } = "";
        public string goodsId { get; set; } = "";
        public string owner { get; set; } = "";
        public DateTime createTime { get; set; } = DateTime.Now;
        public DateTime sendTimeStamp { get; set; } = DateTime.Today;
        public int cntSend { get; set; } = 0;
        public int cntRecv { get; set; } = 0;
        public string tag { get; set; } = "";
    }
}
