﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KeywordManager.Web.Data.Migrations
{
    public partial class addFieldInIdentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "etc",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "manager",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "etc",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "manager",
                table: "AspNetUsers");
        }
    }
}
