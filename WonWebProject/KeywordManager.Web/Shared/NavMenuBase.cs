﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using KeywordManager.Web.Services;
using Microsoft.AspNetCore.Components.Authorization;
using KeywordManager.Model;
using KeywordManager.Web.Model;
using System.Net.Http;

namespace KeywordManager.Web.Shared
{
    public class NavMenuBase : ComponentBase
    {
        protected bool collapseNavMenu = true;

        protected string NavMenuCssClass => collapseNavMenu ? "collapse" : null;

        protected void ToggleNavMenu()
        {
            collapseNavMenu = !collapseNavMenu;
        }

        [CascadingParameter]
        public Task<AuthenticationState> authenticationStateTask { get; set; }
        [Inject]
        protected UserManager<ApplicationUser> uesrManager { get; set; }

        [Inject]
        public IUserPointService uesrPointService { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public IGoodsService goodsService { get; set; }

        protected int point { get; set; } = 0;
        protected string id { get; set; } = "";

        public bool doneToLoad = false;

        public string roleName = "유져";

        protected async override Task OnInitializedAsync()
        {
            var authenticationState = await authenticationStateTask;
            if (authenticationState.User.Identity.IsAuthenticated)
            {
                var currentUser = await uesrManager.GetUserAsync(authenticationState.User);
                var userInfo = await uesrPointService.GetInfo(currentUser.Id);
                point = userInfo.point;
                id = currentUser.Id;
            }

            if (authenticationState.User.IsInRole("Admin"))
                roleName = "관리자";
            else if (authenticationState.User.IsInRole("Manager"))
                roleName = "매니져";

            doneToLoad = true;
        }

        public async Task OnclickTest()
        {
            var ret = await GetLowWokingItem(3);
        }

        static async Task<string> GetLowWokingItem(int cnt_ = 1)
        {
            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
            HttpClient client = new HttpClient(httpClientHandler);

            //client.BaseAddress = new Uri("Https://121.167.252.238:44397/");
            client.BaseAddress = new Uri("Https://localhost:44397/");
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkFkbWluVG9rZW4iLCJuYmYiOjE2MjU1NDM0NzcsImV4cCI6MTk0MTA3NjI3NywiaWF0IjoxNjI1NTQzNDc3fQ.gg3GCJIJGB3PJlNaHEQnD4Xk9tLqFoWxJGH4DV-avyk");

            try
            {
                var encodedContent = new FormUrlEncodedContent(new Dictionary<string, string>());
                var response = await client.PostAsync($"api/Goods/GetLowWokingItem?cnt={cnt_}", encodedContent);
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
