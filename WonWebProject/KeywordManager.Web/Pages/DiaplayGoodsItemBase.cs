﻿using KeywordManager.Components;
using KeywordManager.Model;
using KeywordManager.Web.Services;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Web.Pages
{
    public class DiaplayGoodsItemBase : ComponentBase
    {
        [Parameter]
        public Goods goods { get; set; }

        public bool doEdit { get; set; } = false;
        [Inject]
        public NavigationManager navigationManager { get; set; }
        [Inject]
        public IGoodsService goodsService { get; set; }

        protected ConfirmBase deleteConfirm { get; set; }

        protected void OnclickEdit()
        {
            navigationManager.NavigateTo($"/editgoodsitem/{goods.id}", true);
        }

        public void OnclickDelete()
        {
            deleteConfirm.Show();
        }

        public async void DoDelete(bool value)
        {
            if (value)
            {
                await goodsService.DeleteInfo(goods.id);
                navigationManager.NavigateTo($"/goodsmanaging/{goods.owner}", true);
            }
            
        }
    }
}
