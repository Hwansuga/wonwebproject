﻿using AutoMapper;
using KeywordManager.Components;
using KeywordManager.Model;
using KeywordManager.Web.Model;
using KeywordManager.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeywordManager.Web.Pages
{
    public class ListMemberBase : ComponentBase
    {
        [CascadingParameter]
        public Task<AuthenticationState> authenticationStateTask { get; set; }
        [Inject]
        public IUserPointService userPointService { get; set; }
        [Inject]
        ISlotService slotService { get; set; }
        [Inject]
        IGoodsService goodsService { get; set; }

        [Inject]
        public NavigationManager navigationManager { get; set; }
        [Inject]
        protected UserManager<ApplicationUser> userManager { get; set; }
        [Inject]
        protected IMapper mapper { get; set; }
        [Inject]
        public IJSRuntime jsRuntime { get; set; }

        public List<ViewUserPoint> listViewUserPoints { get; set; } = null;

        public Dictionary<ViewUserPoint, List<ViewUserPoint>> dicManager = new Dictionary<ViewUserPoint, List<ViewUserPoint>>();
        public List<ViewUserPoint> listUserNoManager = new List<ViewUserPoint>();
        protected PopupMsgBase msgBox { get; set; }

        protected bool loadDone = false;
        
        protected List<ApplicationUser> listManager { get; set; }
        protected int totalSlot = 0;
        protected async override Task OnInitializedAsync()
        {

            try
            {
                var authenticationState = await authenticationStateTask;

                listManager = (await userManager.GetUsersInRoleAsync("Manager")).ToList();
                await Task.Delay(1);

                var usersNoManager = (await userManager.GetUsersInRoleAsync("User")).ToList().FindAll(x => string.IsNullOrEmpty(x.manager));

                listUserNoManager = new List<ViewUserPoint>();
                foreach (var user in usersNoManager)
                    listUserNoManager.Add(await GetUserPoint(user));
                   
                listUserNoManager.AddRange(await GetUserWithNoRole());
                listUserNoManager = listUserNoManager.OrderBy(x => x.name).ToList();

                var listUser = await userManager.GetUsersInRoleAsync("Manager");
                listUser = listUser.OrderBy(x => x.Email).ToList();
                foreach (var user in listUser)
                {
                    var listCustomer = (await userManager.Users.ToListAsync()).FindAll(x => x.manager == user.Email);
                    listCustomer = listCustomer.OrderBy(x => x.Email).ToList();

                    var listUserPoint = new List<ViewUserPoint>();
                    foreach(var customer in listCustomer)
                    {
                        listUserPoint.Add(await GetUserPoint(customer));
                    }
                    dicManager.Add(await GetUserPoint(user), listUserPoint.ToList());
                }

                totalSlot = (await slotService.GetInfos()).Count();

                loadDone = true;
            }
            catch(Exception ex)
            {
                Console.WriteLine($"ex {ex.Message}");
                navigationManager.NavigateTo("/listmember", true);
            }
            
        }

        async Task<IList<ViewUserPoint>> GetUserWithNoRole()
        {
            var listUser = new List<ViewUserPoint>();
            foreach(var user in userManager.Users)
            {
                var roles = await userManager.GetRolesAsync(user);
                if (roles.Count <= 0)
                    listUser.Add(await GetUserPoint(user));
            }
            return listUser;
        }

        async Task<ViewUserPoint> GetUserPoint(ApplicationUser user_)
        {
            var userPoint = await userPointService.GetInfo(user_.Id);

            var viewItem = new ViewUserPoint();
            mapper.Map(userPoint, viewItem);
            viewItem.name = user_.Email;
            viewItem.manager = user_.manager;
            viewItem.etc = user_.etc;
            viewItem.company = user_.company;
            var listRole = (await userManager.GetRolesAsync(user_));
            if (listRole.Count > 0)
                viewItem.role = listRole[0];

            if (string.IsNullOrEmpty(user_.manager) == false)
            {
                var managerInfo = await userManager.FindByEmailAsync(viewItem.manager);
                var managerUp = await userPointService.GetInfo(managerInfo.Id);
                if (managerInfo != null)
                {
                    viewItem.managerCompany = managerInfo.company;
                    viewItem.memo1 = managerUp.memo1;
                    viewItem.memo2 = managerUp.memo2;
                }
            }

            return viewItem;
        }

        protected void OnclickDel(ViewUserPoint info_)
        {
            msgBox.title = "알림";
            var sb = new StringBuilder();
            sb.Append($"<b>업체명</b> : {info_.company}<br/>");

            if (Util.IsExpireDigitDay(info_.expire))
                sb.Append($"<b>만료기간</b> : <font color='red'>{info_.expire.ToString("yyyy-MM-dd")}</font><br/>");
            else
                sb.Append($"<b>만료기간</b> : {info_.expire.ToString("yyyy-MM-dd")}<br/>");
            sb.Append($"삭제 하시겠습니까?<br/>");

            msgBox.msg = new MarkupString(sb.ToString());
            msgBox.funcOk = async () =>
            {
                await DeleteMember(info_.id);
                //await DownloadCurFile();
            };

            msgBox.SetShow();
        }

        async Task CancellationManager(string email_)
        {
            var listUser = await userManager.Users.Where(x => x.manager == email_).ToListAsync();
            listUser.ForEach(x => x.manager = string.Empty);
            foreach (var user in listUser)
                await userManager.UpdateAsync(user);
        }

        protected void OnclickDelManager(ViewUserPoint info_)
        {
            msgBox.title = "알림";
            var sb = new StringBuilder();
            sb.Append($"<b>업체명</b> : {info_.company}<br/>");
            sb.Append($"해당 매니져를 삭제 하시겠습니까?<br/>");

            msgBox.msg = new MarkupString(sb.ToString());
            msgBox.funcOk = async () =>
            {
                var targetUser = await userManager.FindByIdAsync(info_.id);
                await CancellationManager(targetUser.Email);
                await DeleteMember(info_.id);
            };

            msgBox.SetShow();
        }

        public async Task DeleteMember(string id)
        {
            await slotService.DeleteByOwner(id);
            await goodsService.DeleteByOwner(id);
            await userPointService.DeleteUserPoint(id);

            await Task.Delay(10);

            var identityUser = await userManager.FindByIdAsync(id);
            if (identityUser != null)
            {                      
                var result = await userManager.DeleteAsync(identityUser);

                if (result != null)
                {
                    navigationManager.NavigateTo("/listmember" , true);
                }
            }
        }

        public async void OnChange(DateTime? value, string id, string name, string format)
        {
            var result = await userPointService.GetInfo(id);
            if (result != null)
            {
                DateTime d = (DateTime)value;
                result.expire = new DateTime(d.Year, d.Month, d.Day, 23, 59, 59);
                await userPointService.UpdateUserPoint(result);
            }

            msgBox.title = "알림";
            var sb = new StringBuilder();
            if (Util.IsExpireDigitDay(result.expire))
                sb.Append($"<b>만료기간</b> : <font color='red'>{result.expire.ToString("yyyy-MM-dd")}</font><br/>");
            else
                sb.Append($"<b>만료기간</b> : {result.expire.ToString("yyyy-MM-dd")}<br/>");
            sb.Append($"로 변경 되었습니다.");

            msgBox.msg = new MarkupString(sb.ToString());
            msgBox.captinoCancel = "확인";
            msgBox.visibleOk = false;

            msgBox.SetShow();
        }

        public async void UpdateManager(object value, string id)
        {
            var targeUser = await userManager.FindByIdAsync(id);
            targeUser.manager = value.ToString();
            var ret = await userManager.UpdateAsync(targeUser);

            if (ret.Succeeded)
            {
                msgBox.title = "알림";
                var sb = new StringBuilder();
                sb.AppendLine($"<b>{targeUser.Email}<b>의 매니져가 <b>{targeUser.manager}<b>로 변경되었습니다.");

                msgBox.msg = new MarkupString(sb.ToString());
                msgBox.visibleCancel = false;

                msgBox.funcOk = async () => {
                    navigationManager.NavigateTo("/listmember", true);
                };


                msgBox.SetShow();
            }
            else
            {
                msgBox.title = "에러";
                var sb = new StringBuilder();
                sb.AppendLine($"매니져 변경이 실패 하였습니다.");

                msgBox.msg = new MarkupString(sb.ToString());
                msgBox.captinoCancel = "확인";
                msgBox.visibleOk = false;

                msgBox.SetShow();
            }

            
        }

        public async Task UpdateUP(UserPoint _up)
        {
            var ret = await userPointService.UpdateUserPoint(_up);
            if (ret != null)
            {
                await jsRuntime.InvokeAsync<object>("Alert", "저정 되었습니다.");
            }
        }
    }
}
