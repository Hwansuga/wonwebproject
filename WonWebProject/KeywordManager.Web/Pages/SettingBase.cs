﻿using KeywordManager.Web.Services;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KeywordManager.Model;

namespace KeywordManager.Web.Pages
{
    public class SettingBase : ComponentBase
    {
        [Inject]
        ISettingService settingService { get; set; }
        [Inject]
        public NavigationManager navigationManager { get; set; }

        public KeywordManager.Model.Setting setting { get; set; } = null;

        protected async override Task OnInitializedAsync()
        {
            setting = await settingService.GetInfo(1);
        }

        protected async Task HandleValidSubmit()
        {
            var result = await settingService.UpdateInfo(setting);

            if (result != null)
            {
                navigationManager.NavigateTo("/");
            }
        }

    }
}
