﻿using AutoMapper;
using KeywordManager.Web.Model;
using KeywordManager.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace KeywordManager.Web.Pages
{
    public class AllSlotBase : ComponentBase
    {
        [CascadingParameter]
        public Task<AuthenticationState> authenticationStateTask { get; set; }
        [Inject]
        public NavigationManager navigationManager { get; set; }
        [Inject]
        public IGoodsService goodsService { get; set; }
        [Inject]
        public ISlotService slotService { get; set; }
        [Inject]
        public IMapper mapper { get; set;  }
        [Inject]
        protected UserManager<ApplicationUser> uesrManager { get; set; }

        public Dictionary<ApplicationUser, List<ViewSlot>> dicSlot { get; set; } = new Dictionary<ApplicationUser, List<ViewSlot>>();

        public bool donToInit = false;

        protected async override Task OnInitializedAsync()
        {
            var authenticationState = await authenticationStateTask;
            if (!authenticationState.User.Identity.IsAuthenticated)
            {
                string returnUrl = WebUtility.UrlEncode($"/allgoods");
                navigationManager.NavigateTo($"/identity/account/login?returnUrl={returnUrl}");
            }

            try
            {
                var listAllUser = await uesrManager.Users.ToListAsync();
                foreach (var user in listAllUser)
                {
                    var listSlot = (await slotService.Search(user.Id)).ToList();
                    List<ViewSlot> listViewSlot = new List<ViewSlot>();
                    foreach (var itemSlot in listSlot)
                    {

                        ViewSlot itemViewSlot = new ViewSlot();
                        mapper.Map(itemSlot, itemViewSlot);
                        if (string.IsNullOrEmpty(itemSlot.goodsId) == false)
                        {
                            var goods = await goodsService.GetInfo(itemSlot.goodsId);
                            if (goods != null)
                                itemViewSlot.goods = goods;
                        }

                        listViewSlot.Add(itemViewSlot);
                    }
                    dicSlot.Add(user, listViewSlot);
                }
                donToInit = true;
            }
            catch(Exception ex)
            {
                navigationManager.NavigateTo($"/allslot", true);
                return;
            }

            
        }
    }
}
