﻿using AutoMapper;
using BlazorInputFile;
using ExcelDataReader;
using KeywordManager.Components;
using KeywordManager.Model;
using KeywordManager.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace KeywordManager.Web.Pages
{
    public class GoodsManagingBase : ComponentBase
    {
        [CascadingParameter]
        public Task<AuthenticationState> authenticationStateTask { get; set; }
        [Inject]
        public NavigationManager navigationManager { get; set; }
        [Inject]
        public IMapper mapper { get; set; }
        [Inject]
        public IGoodsService goodsService { get; set; }
        [Inject]
        IWebHostEnvironment Environment { get; set; }
        //[Inject]
        //ILogger<FileUpload1> Logger { get; set; }

        [Parameter]
        public string owner { get; set; }

        public Goods goods { get; set; } = new Goods();

        public List<Goods> listGoods { get; set; } = null;

        protected ConfirmBase msgBox { get; set; }
        protected string msgBoxTitle { get; set; } = "";
        protected string msgBoxMsg { get; set; } = "";

        public bool donToInit = false;
        

        protected async override Task OnInitializedAsync()
        {
            var authenticationState = await authenticationStateTask;
            if (!authenticationState.User.Identity.IsAuthenticated)
            {
                string returnUrl = WebUtility.UrlEncode($"/goodsmanaging/{owner}");
                navigationManager.NavigateTo($"/identity/account/login?returnUrl={returnUrl}");
            }

            goods.id = Guid.NewGuid().ToString();
            goods.owner = owner;

            listGoods = (await goodsService.Search(owner)).ToList();
            donToInit = true;
        }

        protected async Task AddGoods()
        {
            var temp = new Goods();
            mapper.Map(goods, temp);

            var result = await goodsService.AddInfo(temp);
            if (result != null)
            {
                navigationManager.NavigateTo($"/goodsmanaging/{owner}" , true);
            }
        }

        public bool uploading = false;
        protected  async Task LoadFiles(IFileListEntry[] files)
        {
            uploading = true;
            var file = files.FirstOrDefault();
            if (file != null)
            {
                // Just load into .NET memory to show it can be done
                // Alternatively it could be saved to disk, or parsed in memory, or similar
                var ms = new MemoryStream();
                await file.Data.CopyToAsync(ms);
                try
                {
                    var excelReader = file.Name.EndsWith(".xls")
                    ? ExcelReaderFactory.CreateBinaryReader(ms)
                    : ExcelReaderFactory.CreateOpenXmlReader(ms);
                    DataSet resultDataSet = excelReader.AsDataSet();

                    var table = resultDataSet.Tables[0];

                    Dictionary<string, int> dicColumn = new Dictionary<string, int>();
                    dicColumn.Add("keyword" , -1);
                    dicColumn.Add("value1", -1);
                    dicColumn.Add("value2", -1);
                    dicColumn.Add("option", -1);
                    dicColumn.Add("memo", -1);
                    dicColumn.Add("goodsName", -1);
                    dicColumn.Add("customer", -1);

                    var listNeedField = GetColoum(table , ref dicColumn);
                    if (listNeedField.Count <= 0)
                    {
                        var listGoodsFromFile = GetGoodsFromTable(table , dicColumn);
                        var listAdd = listGoodsFromFile.Where(x => listGoods.All(y => IsSameItem(x, y) == false)).ToList();
                       
                        if (listAdd.Count > 0)
                        {
                            var addReslut = (await goodsService.AddInfos(listAdd)).ToList();
                            ShowMsgBox("알림", $"{string.Join(',', addReslut.Select(x => x.goodsName).ToArray())} 상품 등록",
                                ()=> {
                                    navigationManager.NavigateTo($"/goodsmanaging/{owner}", true);
                                });
                                                                  
                        }
                        else
                        {
                            ShowMsgBox("알림", "추가된 상품이 없습니다.");
                        }
                            

                    }
                    else
                    {
                        ShowMsgBox("에러", "{string.Join(',',listNeedField.ToArray())} 컬럼 정보가 없습니다.");
                    }

                    //string status = $"Finished loading {file.Size} bytes from {file.Name}";
                }
                catch (Exception ex)
                {
                    ShowMsgBox("에러" , "파일을 확인해 주세요");
                }
                
            }

            uploading = false;
        }

        List<string> GetColoum(DataTable dt , ref Dictionary<string, int> dicColumn)
        {
            List<string> needField = new List<string>();
            var fields = dt.Rows[0].ItemArray.ToList().ConvertAll(x => x.ToString());
            foreach(var item in dicColumn)
            {
                var index = fields.FindIndex(x => x == item.Key);
                if (index == -1)
                    needField.Add(item.Key);

                dicColumn[item.Key] = fields.FindIndex(x => x == item.Key);
            }

            return needField;
        }

        List<Goods> GetGoodsFromTable(DataTable dt , Dictionary<string, int> dicColumn)
        {
            List<Goods> listNewGoods = new List<Goods>();
            for(int i=1; i<dt.Rows.Count; ++i)
            {
                var rowItem = dt.Rows[i].ItemArray.ToList().ConvertAll(x=>x.ToString());
                Goods tempItem = new Goods();
                var listFields = tempItem.GetType().GetProperties().ToList();
                foreach (var columnInfo in dicColumn)
                {
                    var field = listFields.Find(x => x.Name == columnInfo.Key);
                    var value = rowItem[columnInfo.Value];
                    field.SetValue(tempItem , value);
                }
                tempItem.id = Guid.NewGuid().ToString();
                tempItem.owner = owner;
                listNewGoods.Add(tempItem);
            }

            return listNewGoods;
        }

        bool IsSameItem(Goods a , Goods b)
        {
            if (a.keyword != b.keyword)
                return false;
            if (a.value1 != b.value1)
                return false;
            if (a.value2 != b.value2)
                return false;
            if (a.goodsName != b.goodsName)
                return false;

            return true;
        }

        void ShowMsgBox(string title, string msg, Action actino = null)
        {
            msgBoxTitle = title;
            msgBoxMsg = msg;
            msgBox.action = actino;
            msgBox.captionCancel = "확인";
            msgBox.showBtnChange = false;
            msgBox.Show();
        }
    }
}
