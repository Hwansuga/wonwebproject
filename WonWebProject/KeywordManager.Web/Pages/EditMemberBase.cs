﻿using AutoMapper;
using KeywordManager.Components;
using KeywordManager.Model;
using KeywordManager.Web.Model;
using KeywordManager.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace KeywordManager.Web.Pages
{
    public class EditMemberBase : ComponentBase
    {
        [CascadingParameter]
        public Task<AuthenticationState> authenticationStateTask { get; set; }
        [Inject]
        public NavigationManager navigationManager { get; set; }
        [Inject]
        public IUserPointService userPointService { get; set; }
        [Inject]
        protected UserManager<ApplicationUser> userManager { get; set; }
        [Inject]
        public ISlotService slotService { get; set; }
        [Inject]
        IGoodsService goodsService { get; set; }

        [Parameter]
        public string id { get; set; }

        public bool donToInit = false;

        public ApplicationUser identityUser { get; set; }
        public UserPoint userPoint { get; set; }
        public IList<ApplicationUser> listManager { get; set; } = new List<ApplicationUser>();
        protected PopupMsgBase msgBox { get; set; }


        protected async override Task OnInitializedAsync()
        {
            var authenticationState = await authenticationStateTask;

            if (!authenticationState.User.Identity.IsAuthenticated 
                || !authenticationState.User.IsInRole("Admin"))
            {
                string returnUrl = WebUtility.UrlEncode($"/editmember/{id}");
                navigationManager.NavigateTo($"/identity/account/login?returnUrl={returnUrl}");
            }

            identityUser = await userManager.FindByIdAsync(id);
            userPoint = await userPointService.GetInfo(id);
            listManager = await userManager.GetUsersInRoleAsync("Manager");

            donToInit = true;
        }

        public void OnclickUpdate()
        {
            msgBox.title = "알림";
            var sb = new StringBuilder();
            sb.Append($"<b>유져</b> : {identityUser.Email}<br/>");
            sb.Append($"변경사항 적용하시겠습니까?");

            msgBox.msg = new MarkupString(sb.ToString());

            msgBox.funcOk = async () =>
            {
                var retIdentityUser = await userManager.UpdateAsync(identityUser);
                if ((await userManager.IsInRoleAsync(identityUser, "User")) == false)
                {
                    var retRole = await userManager.AddToRoleAsync(identityUser, "User");
                }             
                var retUserPoint = await userPointService.UpdateUserPoint(userPoint);

                var listSlot = (await slotService.Search(retUserPoint.id)).ToList();
                if (listSlot.Count > retUserPoint.cntSlot)
                {
                    var cntDel = listSlot.Count - retUserPoint.cntSlot;
                    listSlot = listSlot.OrderBy(x => x.createTime).ToList();
                    listSlot = listSlot.GetRange(retUserPoint.cntSlot , cntDel);
                    foreach(var slot in listSlot)
                    {
                        if (string.IsNullOrEmpty(slot.goodsId) == false)
                            await goodsService.DeleteInfo(slot.goodsId);

                        await slotService.DeleteInfo(slot.id);
                    }

                }
                else if (listSlot.Count < retUserPoint.cntSlot)
                {
                    var cntAdd = retUserPoint.cntSlot - listSlot.Count;
                    var addSlots = await slotService.AddSlot(retUserPoint.id, cntAdd);
                }


                navigationManager.NavigateTo("/listmember");
            };

            msgBox.SetShow();
        }
    }
}
