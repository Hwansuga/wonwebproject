﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.JSInterop;
using System;
using AutoMapper;
using KeywordManager.Model;
using KeywordManager.Web.Services;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using KeywordManager.Components;
using System.Text;

namespace KeywordManager.Web.Pages
{
    public class PopupRegistSlotBase : ComponentBase
    {

        public PopupMsgBase popupMsg { get; set; }
        
        [Inject]
        public NavigationManager navigationManager { get; set; }
        [Inject]
        public IMapper mapper { get; set; }
        [Inject]
        public IGoodsService goodsService { get; set; }
        [Inject]
        public ISlotService slotService { get; set; }
        [Inject]
        public IJSRuntime jsRuntime { get; set; }
        public Goods goods { get; set; } = null;

        protected bool show { get; set; } = false;

        public Func<Task> funSuccess = null;
        public Func<Task> funFailed = null;
        public string owner { get; set; } = "";
        public Slot targetSlot = null;

        public string caption = "등록";

        protected async Task RegistSlot()
        {
            if (targetSlot == null)
                return;

            if (string.IsNullOrEmpty(goods.keyword))
            {
                await jsRuntime.InvokeAsync<object>("Alert", "키워드를 입력해 주세요");
                return;
            }

            //regist
            if (string.IsNullOrEmpty(goods.id))
            {
                goods.id = Guid.NewGuid().ToString();
                goods.owner = owner;

                var result = await goodsService.AddInfo(goods);
                if (result != null)
                {
                    targetSlot.goodsId = result.id;

                    var retSlot = await slotService.UpdateInfo(targetSlot);
                }
            }
            else //edit
            {
                var result = await goodsService.UpdateInfo(goods);
            }

            if (funSuccess != null)
            {
                await funSuccess();
            }
            navigationManager.NavigateTo($"/slotmanaging/{goods.owner}", true);
        }

        protected void OnclickCancel()
        {
            show = false;
            StateHasChanged();
        }
        public void SetShow()
        {
            show = true;
            StateHasChanged();
        }
    }
}
