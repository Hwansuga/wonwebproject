﻿using AutoMapper;
using KeywordManager.Model;
using KeywordManager.Web.Model;
using KeywordManager.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace KeywordManager.Web.Pages
{
    public class AllGoodsBase : ComponentBase
    {
        [CascadingParameter]
        public Task<AuthenticationState> authenticationStateTask { get; set; }
        [Inject]
        public NavigationManager navigationManager { get; set; }
        [Inject]
        public IGoodsService goodsService { get; set; }
        [Inject]
        public UserManager<ApplicationUser> userManager { get; set; }
        [Inject]
        protected ISlotService slotService { get; set; }
        [Inject]
        IUserPointService userPointService { get; set; }
        [Inject]
        protected IMapper mapper { get; set; }

        public Dictionary<ViewUserPoint, List<KeyValuePair<Slot, Goods>>> dicGoods = new Dictionary<ViewUserPoint, List<KeyValuePair<Slot, Goods>>>();

        public bool donToInit = false;



        protected int cntSlotNoTag = 0;

        protected async override Task OnInitializedAsync()
        {
            var authenticationState = await authenticationStateTask;
            if (!authenticationState.User.Identity.IsAuthenticated)
            {
                string returnUrl = WebUtility.UrlEncode($"/allgoods");
                navigationManager.NavigateTo($"/identity/account/login?returnUrl={returnUrl}");
            }

            try
            {
                var me = authenticationState.User;
                if (me.IsInRole("Admin") == false && me.IsInRole("Manager") == false)
                    return;

                cntSlotNoTag = await GetCountWithoutTagging();

                var listAllUser = await userManager.Users.ToListAsync();

                foreach (var user in listAllUser)
                {
                    if (user.UserName == me.Identity.Name)
                        continue;

                    if (me.IsInRole("Manager"))
                    {
                        if (user.manager != me.Identity.Name)
                            continue;
                    }

                    var listSlot = (await slotService.Search(user.Id)).ToList();
                    listSlot = listSlot.OrderBy(x => x.createTime).ToList();
                    var dicSlot = new List<KeyValuePair<Slot, Goods>>();
                    foreach(var slot in listSlot)
                    {
                        if (string.IsNullOrEmpty(slot.goodsId))
                        {
                            dicSlot.Add(new KeyValuePair<Slot, Goods>(slot, null));
                        }
                        else
                        {
                            var targetGoods = await goodsService.GetInfo(slot.goodsId);
                            dicSlot.Add(new KeyValuePair<Slot, Goods>(slot, targetGoods));
                        }
                        
                    }

                    dicGoods.Add( await GetUserPoint(user), dicSlot);
                }
                dicGoods = dicGoods.OrderBy(x => x.Key.managerCompany).ThenBy(x => x.Key.name).ToDictionary(x=>x.Key ,x=>x.Value);

                dicGoods = dicGoods.Where(x => string.IsNullOrEmpty(x.Key.manager) == false).ToDictionary(x => x.Key, x => x.Value);

                donToInit = true;
            }
            catch(Exception ex)
            {
                navigationManager.NavigateTo($"/allgoods", true);
                return;
            }
            
        }

        async Task<ViewUserPoint> GetUserPoint(ApplicationUser user_)
        {
            var userPoint = await userPointService.GetInfo(user_.Id);

            var viewItem = new ViewUserPoint();
            mapper.Map(userPoint, viewItem);
            viewItem.name = user_.Email;
            viewItem.manager = user_.manager;
            viewItem.etc = user_.etc;
            viewItem.company = user_.company;
            var listRole = (await userManager.GetRolesAsync(user_));
            if (listRole.Count > 0)
                viewItem.role = listRole[0];


            if (string.IsNullOrEmpty(user_.manager) == false)
            {
                var managerInfo = await userManager.FindByEmailAsync(viewItem.manager);
                if (managerInfo != null)
                {
                    viewItem.managerCompany = managerInfo.company;
                }
            }

            return viewItem;
        }

        protected async Task<int> GetCountWithoutTagging()
        {
            var slots = await slotService.GetInfos();
            return slots.ToList().FindAll(x=>string.IsNullOrEmpty(x.tag)).Count;
        }
    }
}
