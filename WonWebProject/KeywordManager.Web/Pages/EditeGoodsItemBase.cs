﻿using AutoMapper;
using KeywordManager.Model;
using KeywordManager.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace KeywordManager.Web.Pages
{
    public class EditeGoodsItemBase : ComponentBase
    {
        [CascadingParameter]
        public Task<AuthenticationState> authenticationStateTask { get; set; }
        [Inject]
        public NavigationManager navigationManager { get; set; }
        [Inject]
        public IMapper mapper { get; set; }
        [Inject]
        public IGoodsService goodsService { get; set; }

        [Parameter]
        public string id { get; set; }

        public Goods goods { get; set; }

        public bool donToInit = false;
        protected async override Task OnInitializedAsync()
        {
            var authenticationState = await authenticationStateTask;
            if (!authenticationState.User.Identity.IsAuthenticated)
            {
                string returnUrl = WebUtility.UrlEncode($"/goodsmanaging/{goods.owner}");
                navigationManager.NavigateTo($"/identity/account/login?returnUrl={returnUrl}");
            }

            goods = await goodsService.GetInfo(id);

            donToInit = true;
        }


        protected async Task UpdateGoods()
        {
            var temp = new Goods();
            mapper.Map(goods, temp);

            var result = await goodsService.UpdateInfo(temp);
            if (result != null)
            {
                navigationManager.NavigateTo($"/goodsmanaging/{temp.owner}");
            }
        }

        protected void OnclickCancel()
        {
            navigationManager.NavigateTo($"/goodsmanaging/{goods.owner}");
        }
    }
}
