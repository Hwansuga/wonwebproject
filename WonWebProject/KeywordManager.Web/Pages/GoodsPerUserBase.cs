﻿using AutoMapper;
using KeywordManager.Model;
using KeywordManager.Web.Model;
using KeywordManager.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace KeywordManager.Web.Pages
{
    public class GoodsPerUserBase : ComponentBase
    {
        [Parameter]
        public ViewUserPoint userInfo { get; set; }

        [Parameter]
        public List<KeyValuePair<Slot, Goods>> dicSlot { get; set; }

        [Parameter]
        public bool hideGoods { get; set; } = true;

        [Parameter]
        public bool showBtn { get; set; } = true;

        [Parameter]
        public bool showCntValue { get; set; } = true;

        [Parameter]
        public bool hideBtnAction { get; set; } = true;

        [Parameter]
        public Action<Goods> actionDel { get; set; } = null;

        [Parameter]
        public Action<Slot> actionReg { get; set; } = null;
        [Parameter]
        public Action<Slot , Goods> actionEdit { get; set; } = null;

        [Inject]
        public UserManager<ApplicationUser> uesrManager { get; set; }

        protected string captionBtn { get; set; } = "펼쳐보기";

        protected string captionOrderGoods { get; set; } = "정렬방식";

        protected override void OnInitialized()
        {
            this.UpdateBtnCaption();
        }

        void UpdateBtnCaption()
        {
            if (hideGoods)
            {
                captionBtn = "펼쳐보기";
            }
            else
            {
                captionBtn = "감추기";
            }
        }

        public MarkupString PrintExpireDate()
        {
            if (Util.IsExpireDigitDay(userInfo.expire))
                return new MarkupString($"<b><font color='red'>{userInfo.expire.ToString("yyyy-MM-dd")}</font></b>");
            else
                return new MarkupString($"{userInfo.expire.ToString("yyyy-MM-dd")}");
        }

        public void OnclickShowHide()
        {
            hideGoods = !hideGoods;
            this.UpdateBtnCaption();
        }

        public void SortValue1()
        {
            captionOrderGoods = "1차값";
            var listNull = dicSlot.FindAll(x => x.Value == null);
            var listNotNull = dicSlot.FindAll(x => x.Value != null);
            listNotNull = listNotNull.OrderBy(x => x.Value.value1).ToList();
            listNotNull.AddRange(listNull);
            dicSlot = listNotNull;
        }

        public void SortValue2()
        {
            captionOrderGoods = "2차값";
            var listNull = dicSlot.FindAll(x => x.Value == null);
            var listNotNull = dicSlot.FindAll(x => x.Value != null);
            listNotNull = listNotNull.OrderBy(x => x.Value.value2).ToList();
            listNotNull.AddRange(listNull);
            dicSlot = listNotNull;
        }
    }
}
