﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace KeywordManager.Web.Pages
{
    public class KeywordEditBase : ComponentBase
    {
        [CascadingParameter]
        public Task<AuthenticationState> authenticationStateTask { get; set; }

        [Parameter]
        public string Id { get; set; }

        [Inject]
        public NavigationManager navigationManager { get; set; }
        protected async override Task OnInitializedAsync()
        {
            var authenticationState = await authenticationStateTask;

            if (authenticationState.User.Identity.IsAuthenticated == false)
            {
                string returnUrl = WebUtility.UrlEncode($"/KeywordEdit/{Id}");
                navigationManager.NavigateTo($"/identity/account/login?returnUrl={returnUrl}");
            }
        }
    }
}
