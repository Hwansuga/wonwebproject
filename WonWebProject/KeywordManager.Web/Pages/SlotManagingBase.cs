﻿using AutoMapper;
using BlazorInputFile;
using CsvHelper;
using CsvHelper.Configuration;
using KeywordManager.Components;
using KeywordManager.Model;
using KeywordManager.Web.Model;
using KeywordManager.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace KeywordManager.Web.Pages
{
    public class SlotManagingBase : ComponentBase
    {
        [CascadingParameter]
        public Task<AuthenticationState> authenticationStateTask { get; set; }
        [Inject]
        public NavigationManager navigationManager { get; set; }
        [Inject]
        protected UserManager<ApplicationUser> userManager { get; set; }
        [Inject]
        public IMapper mapper { get; set; }
        [Inject]
        public ISlotService slotService { get; set; }

        [Inject]
        public ISettingService settingService { get; set; }
        [Inject]
        public IUserPointService userPointService { get; set; }
        [Inject]
        public IGoodsService goodsService { get; set; }
        [Inject]
        public IJSRuntime jsRuntime { get; set; }
        [Inject]
        Microsoft.AspNetCore.Hosting.IWebHostEnvironment env { get; set; }

        
        public PopupMsgBase popupMsg { get; set; }
        public PopupRegistSlotBase popupRegist { get; set; }

        [Parameter]
        public string owner { get; set; }
        public KeywordManager.Model.Setting setting { get; set; }
        public UserPoint userPoint { get; set; }
        public List<Goods> myGoods { get; set; } = new List<Goods>();
        public List<ViewSlot> listViewslot { get; set; } = new List<ViewSlot>();

        protected ConfirmBase errConfirm { get; set; }

        protected IdentityUser identityUser { get; set; }


        public bool doneToInit = false;


        public ViewUserPoint viewUserPoint { get; set; }

        public List<KeyValuePair<Slot ,Goods>> listSlotGoodsInfo { get; set; }

        protected async override Task OnInitializedAsync()
        {
            var authenticationState = await authenticationStateTask;
            if (!authenticationState.User.Identity.IsAuthenticated)
            {
                string returnUrl = WebUtility.UrlEncode($"/slotmanaging/{owner}");
                navigationManager.NavigateTo($"/identity/account/login?returnUrl={returnUrl}");
            }


            try
            {
                var userInfo = await userManager.FindByIdAsync(owner);
                await Task.Delay(10);
                viewUserPoint = await GetUserPoint(userInfo);

                var listSlot = (await slotService.Search(owner)).ToList();

                if (viewUserPoint.cntSlot > listSlot.Count)
                {
                    var cntAdd = viewUserPoint.cntSlot - listSlot.Count;

                    var addSlots = await slotService.AddSlot(owner , cntAdd);
                    listSlot.AddRange(addSlots); 
                }


                listSlot = listSlot.OrderBy(x => x.createTime).ToList();
                listSlotGoodsInfo = new List<KeyValuePair<Slot, Goods>>();
                foreach (var slot in listSlot)
                {
                    if (string.IsNullOrEmpty(slot.goodsId))
                    {
                        listSlotGoodsInfo.Add(new KeyValuePair<Slot, Goods>(slot, null));
                    }
                    else
                    {
                        var targetGoods = await goodsService.GetInfo(slot.goodsId);
                        listSlotGoodsInfo.Add(new KeyValuePair<Slot, Goods>(slot, targetGoods));
                    }                                       
                }

                doneToInit = true;
            }
            catch(Exception ex)
            {
                navigationManager.NavigateTo($"/slotmanaging/{owner}" , true);
            }
            
        }

        public void OnclickRegGoods(Slot _slot)
        {
            popupRegist.targetSlot = _slot;
            popupRegist.goods = new Goods();
            popupRegist.owner = owner;
            popupRegist.caption = "등록";
            popupRegist.SetShow();
        }

        public void OnclickEdit(Slot _slot , Goods _goods)
        {
            popupRegist.targetSlot = _slot;
            popupRegist.goods = _goods;
            popupRegist.owner = owner;
            popupRegist.caption = "편집";
            popupRegist.SetShow();
        }

        public async void OnclickDelete(string id)
        {
            await slotService.DeleteInfo(id);
            navigationManager.NavigateTo($"/slotmanaging/{owner}", true);
        }

        public async void OnclickUpdate(ViewSlot slot)
        {
            Slot slotToUpdate = new Slot();
            mapper.Map(slot, slotToUpdate);
            var result = await slotService.UpdateInfo(slotToUpdate);
            if (result != null)
            {
                navigationManager.NavigateTo($"/slotmanaging/{owner}", true);
            }
        }
        async Task<ViewUserPoint> GetUserPoint(ApplicationUser user_)
        {
            var userPoint = await userPointService.GetInfo(user_.Id);

            var viewItem = new ViewUserPoint();
            mapper.Map(userPoint, viewItem);
            viewItem.name = user_.Email;
            viewItem.manager = user_.manager;
            viewItem.etc = user_.etc;
            viewItem.company = user_.company;
            var listRole = (await userManager.GetRolesAsync(user_));
            if (listRole.Count > 0)
                viewItem.role = listRole[0];

            if (string.IsNullOrEmpty(user_.manager) == false)
            {
                var managerInfo = await userManager.FindByEmailAsync(viewItem.manager);
                var managerUp = await userPointService.GetInfo(managerInfo.Id);
                if (managerInfo != null)
                {
                    viewItem.managerCompany = managerInfo.company;
                    viewItem.memo1 = managerUp.memo1;
                    viewItem.memo2 = managerUp.memo2;
                }
            }
            

            return viewItem;
        }

        public void OnclickDownloadSample()
        {
            var fileName = $@"{env.WebRootPath}\resource\keyword_Sample.csv";
            Util.DownloadFile(jsRuntime, fileName);
        }

        public void OnclickInit()
        {
            popupMsg.title = "알림";
            popupMsg.msg = new MarkupString("모든 슬롯을 초기화 하시겠습니까?");
            popupMsg.funcOk = async () => {
                doneToInit = false;
                StateHasChanged();

                await goodsService.DeleteByOwner(owner);
                var slots = await slotService.Search(owner);
                foreach(var slot in slots)
                {
                    slot.goodsId = string.Empty;                   
                }

                await slotService.UpdateInfos(slots.ToArray());

                navigationManager.NavigateTo($"/slotmanaging/{owner}", true);
            };

            popupMsg.SetShow();
        }

        public async Task OnclickDownload()
        {
            var fileName = $@"{env.WebRootPath}\resource\KeywordData.csv";
            var fileInfo = new FileInfo(fileName);
            if (fileInfo.Exists)
                fileInfo.Delete();

            if (fileInfo.Directory.Exists == false)
                fileInfo.Directory.Create();

            using (var writer = new StreamWriter(fileInfo.Name, false, Encoding.UTF8))
            using (var csv = new CsvWriter(writer, new CsvConfiguration(CultureInfo.InvariantCulture) { HasHeaderRecord = true }))
            {
                await csv.WriteRecordsAsync(listSlotGoodsInfo.Where(t => t.Value != null).Select(x=> new {
                    x.Value.keyword , 
                    x.Value.value1,
                    x.Value.value2,
                    x.Value.option,
                    x.Value.memo,
                    x.Value.searchKeyword,
                    x.Value.rank1,
                    x.Value.rank2
                }));
            }

            Util.DownloadFile(jsRuntime, fileInfo.Name);
        }

        async Task<bool> UpdateSlotInfo(IFileListEntry file_)
        {
            try
            {               
                using (var reader = new StreamReader(file_.Data))
                using (var csv = new CsvReader(reader, new CsvConfiguration(CultureInfo.InvariantCulture) { 
                    HasHeaderRecord = true
                }))
                {                  
                    csv.Context.RegisterClassMap<GoodsMap>();
                    var iter = csv.GetRecordsAsync<Goods>();
                    var listSlot = (await slotService.Search(owner)).ToList();
                    listSlot = listSlot.OrderBy(x => x.createTime).ToList();

                    var listUpdateTarget = new List<Slot>();
                    await foreach (var record in iter)
                    {
                        var tempGoods = record;
                        tempGoods.id = Guid.NewGuid().ToString();
                        tempGoods.owner = owner;

                        var retGoods = await goodsService.AddInfo(tempGoods);

                        var targetSlot = listSlot.FirstOrDefault(x => string.IsNullOrEmpty(x.goodsId));
                        if (targetSlot == null)
                            break;

                        targetSlot.goodsId = tempGoods.id;
                        listUpdateTarget.Add(targetSlot);                       
                    }

                    var retSlot = await slotService.UpdateInfos(listUpdateTarget.ToArray());
                }

                return true;
            }
            catch(Exception ex)
            {
                await jsRuntime.InvokeAsync<object>($"Alert", $"파일 적용 실패.{ex.Message}");

                return false;
            }
        }

        public bool uploading = false;
        protected async Task LoadFiles(IFileListEntry[] files)
        {
            uploading = true;
            var file = files.FirstOrDefault();
            if (file != null)
            {
                if (file.Name.EndsWith(".csv") == false)
                {
                    popupMsg.title = "알림";
                    popupMsg.msg = new MarkupString("csv 파일이 아닙니다.");
                    popupMsg.visibleCancel = false;
                    popupMsg.SetShow();
                    return;
                }

                popupMsg.title = "알림";
                popupMsg.msg = new MarkupString($"{file.Name}을 적용하시겠습니까?");
                popupMsg.funcOk = async () =>
                {
                     doneToInit = false;
                     StateHasChanged();

                    await goodsService.DeleteByOwner(owner);
                    var slots = await slotService.Search(owner);
                    foreach (var slot in slots)
                    {
                        slot.goodsId = string.Empty;
                    }
                    await slotService.UpdateInfos(slots.ToArray());

                    await UpdateSlotInfo(file);

                    navigationManager.NavigateTo($"/slotmanaging/{owner}", true);
                };
                popupMsg.SetShow();
            }

            uploading = false;
        }

        public void OnclickDelItem(Goods goods_)
        {
            popupMsg.title = "알림";
            popupMsg.msg = new MarkupString($"정말 삭제 하시겠습니까?");
            popupMsg.funcOk = async () =>
            {
                var slot = await slotService.GetInfoByGoodsId(goods_.id);
                if (slot != null)
                {
                    slot.goodsId = string.Empty;
                    slot.tag = string.Empty;
                    await slotService.UpdateInfo(slot);
                }
                await goodsService.DeleteInfo(goods_.id);
                
                navigationManager.NavigateTo($"/slotmanaging/{owner}", true);
            };
            popupMsg.SetShow();
        }
    }
}
