﻿
using Microsoft.JSInterop;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace KeywordManager.Web
{
    public class Util
    {
        public static bool IsExpireDigitDay(DateTime date_)
        {
            if (date_.Date < DateTime.Now.Date)
                return true;
            else
                return false;
        }
        public static string GetStyleDate(DateTime date_)
        {
            if (date_.Date < DateTime.Now.AddDays(-3).Date)
                return "color:red";
            else
                return "color:black";
        }

        public static void DownloadFile(IJSRuntime jsRuntime, string fileName_ )
        {
            var arrByte = File.ReadAllBytes(fileName_);

            if (jsRuntime is IJSUnmarshalledRuntime webAssemblyJSRuntime)
            {
                webAssemblyJSRuntime.InvokeUnmarshalled<string, string, byte[], bool>("BlazorDownloadFileFast", Path.GetFileName(fileName_), "application/octet-stream", arrByte);
            }
            else
            {
                // Fall back to the slow method if not in WebAssembly
                jsRuntime.InvokeVoidAsync("BlazorDownloadFile", Path.GetFileName(fileName_), "application/octet-stream", arrByte);
            }
        }
    }
}
