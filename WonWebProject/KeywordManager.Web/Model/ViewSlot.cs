﻿using KeywordManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Web.Model
{
    public class ViewSlot : Slot
    {
        public Goods goods { get; set; } = new Goods();
    }
}
