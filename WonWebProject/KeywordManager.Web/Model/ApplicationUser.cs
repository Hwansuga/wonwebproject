﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Web.Model
{
    public class ApplicationUser : IdentityUser
    {
        public string company { get; set; } = "";
        public string etc { get; set; } = "";
        public string manager { get; set; } = "";
    }
}
