﻿using AutoMapper;
using KeywordManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Web.Model
{
    public class SlotProfile : Profile
    {
        public SlotProfile()
        {
            CreateMap<Slot, ViewSlot>();
            CreateMap<ViewSlot, Slot>();
        }
    }
}
