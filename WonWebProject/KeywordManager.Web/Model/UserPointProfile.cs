﻿using AutoMapper;
using KeywordManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Web.Model
{
    public class UserPointProfile : Profile
    {
        public UserPointProfile()
        {
            CreateMap<UserPoint, ViewUserPoint>();
            CreateMap<ViewUserPoint, UserPoint>();
        }      
    }
}
