﻿using KeywordManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Web.Model
{
    public class ViewUserPoint : UserPoint
    {
        public string name { get; set; }
        public string manager { get; set; }
        public string etc { get; set; }
        public string company { get; set; }

        public string role { get; set; } = "";

        public string managerCompany { get; set; } = "";
    }
}
