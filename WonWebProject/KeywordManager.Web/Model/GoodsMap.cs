﻿using CsvHelper.Configuration;
using KeywordManager.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Web.Model
{
    public class GoodsMap : ClassMap<Goods>
    {
        public GoodsMap()
        {
            AutoMap(CultureInfo.InvariantCulture);
            Map(x => x.id).Ignore();
            Map(x => x.rank1).Ignore();
            Map(x => x.rank1Change).Ignore();
            Map(x => x.rank2).Ignore();
            Map(x => x.rank2Change).Ignore();
            Map(x => x.customer).Ignore();
            Map(x => x.owner).Ignore();
            Map(x => x.cntSend).Ignore();
            Map(x => x.cntRecv).Ignore();
            Map(x => x.goodsName).Ignore();
            Map(x => x.state).Ignore();
        }

    }
}
