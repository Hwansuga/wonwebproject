﻿using KeywordManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Web.Services
{
    public interface ISlotService
    {
        Task<IEnumerable<Slot>> GetInfos();
        Task<Slot> GetInfo(string id);
        Task<IEnumerable<Slot>> Search(string owner);

        Task<Slot> UpdateInfo(Slot value);

        Task<IEnumerable<Slot>> UpdateInfos(Slot[] value);
        Task<Slot> AddInfo(Slot value);

        Task<IEnumerable<Slot>> AddSlot(string _owner, int _cnt);
        Task DeleteInfo(string id);

        Task DeleteByOwner(string owner);

        Task<Slot> GetInfoByGoodsId(string goodsId_);
    }
}
