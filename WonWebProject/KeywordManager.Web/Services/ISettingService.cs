﻿using KeywordManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Web.Services
{
    public interface ISettingService
    {
        Task<IEnumerable<Setting>> GetInfos();
        Task<Setting> GetInfo(int version);

        Task<Setting> UpdateInfo(Setting userPoint);
        Task<Setting> AddInfo(Setting userPoint);
        Task DeleteInfo(int version);
    }
}
