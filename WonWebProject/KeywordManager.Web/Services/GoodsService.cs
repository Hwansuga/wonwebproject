﻿using KeywordManager.Model;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace KeywordManager.Web.Services
{
    
    public class GoodsService : IGoodsService
    {
        readonly HttpClient httpClient;
        public GoodsService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<Goods> AddInfo(Goods value)
        {
            return await httpClient.PostJsonAsync<Goods>("api/Goods", value);
        }

        public async Task<IEnumerable<Goods>> AddInfos(List<Goods> values)
        {
            return await httpClient.PostJsonAsync<Goods[]>("api/Goods/AddInfos", values);
        }

        public async Task DeleteInfo(string id)
        {
            await httpClient.DeleteAsync($"api/Goods/{id}");
        }
        public async Task DeleteByOwner(string owner)
        {
            await httpClient.DeleteAsync($"api/Goods/DeleteByOwner?owner={owner}");
        }

        public async Task<Goods> GetInfo(string id)
        {
            return await httpClient.GetJsonAsync<Goods>($"api/Goods/{id}");
        }

        public async Task<IEnumerable<Goods>> GetInfos()
        {
            return await httpClient.GetJsonAsync<Goods[]>("api/Goods");
        }

        public async Task<IEnumerable<Goods>> Search(string owner)
        {
            return await httpClient.GetJsonAsync<Goods[]>($"api/Goods/Search?owner={owner}");
        }

        public async Task<IEnumerable<Goods>> SearchByKeyWord(string keyword_)
        {
            return await httpClient.GetJsonAsync<Goods[]>($"api/Goods/SearchByKeyWord?keyword={keyword_}");
        }

        public async Task<Goods> UpdateInfo(Goods value)
        {
            return await httpClient.PutJsonAsync<Goods>("api/Goods", value);
        }
    }
}
