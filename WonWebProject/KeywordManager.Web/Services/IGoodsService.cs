﻿using KeywordManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Web.Services
{
    public interface IGoodsService
    {
        Task<IEnumerable<Goods>> GetInfos();
        Task<Goods> GetInfo(string id);
        Task<IEnumerable<Goods>> Search(string owner);

        Task<IEnumerable<Goods>> SearchByKeyWord(string keyword_);

        Task<Goods> UpdateInfo(Goods value);
        Task<Goods> AddInfo(Goods value);
        Task<IEnumerable<Goods>> AddInfos(List<Goods> values);
        Task DeleteInfo(string id);

        Task DeleteByOwner(string owner);

    }
}
