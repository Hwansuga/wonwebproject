﻿using KeywordManager.Model;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace KeywordManager.Web.Services
{
    public class SlotService : ISlotService
    {
        readonly HttpClient httpClient;
        public SlotService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<Slot> AddInfo(Slot value)
        {
            return await httpClient.PostJsonAsync<Slot>("api/Slot", value);
        }

        public async Task<IEnumerable<Slot>> AddSlot(string _owner, int _cnt)
        {
            return await httpClient.PostJsonAsync<Slot[]>($"api/Slot/AddSlot/{_owner}/{_cnt}", null);
        }

        public async Task DeleteInfo(string id)
        {
            await httpClient.DeleteAsync($"api/Slot/{id}");
        }

        public async Task DeleteByOwner(string owner)
        {
            await httpClient.DeleteAsync($"api/Slot/DeleteByOwner?owner={owner}");
        }

        public async Task<Slot> GetInfo(string id)
        {
            return await httpClient.GetJsonAsync<Slot>($"api/Slot/{id}");
        }

        public async Task<IEnumerable<Slot>> GetInfos()
        {
            return await httpClient.GetJsonAsync<Slot[]>("api/Slot");
        }

        public async Task<IEnumerable<Slot>> Search(string owner)
        {
            return await httpClient.GetJsonAsync<Slot[]>($"api/Slot/Search?owner={owner}");
        }

        public async Task<Slot> UpdateInfo(Slot value)
        {
            return await httpClient.PutJsonAsync<Slot>("api/Slot", value);
        }

        public async Task<IEnumerable<Slot>> UpdateInfos(Slot[] value)
        {
            return await httpClient.PutJsonAsync<Slot[]>("api/Slot/UpdateSlots", value);
        }

        public async Task<Slot> GetInfoByGoodsId(string goodsId_)
        {
            return await httpClient.GetJsonAsync<Slot>($"api/Slot/SearchGoods?goodsId={goodsId_}");
        }
    }
}
