﻿using KeywordManager.Model;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace KeywordManager.Web.Services
{
    public class UserPointService : IUserPointService
    {
        readonly HttpClient httpClient;

        public UserPointService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }
        public async Task<UserPoint> AddUserPoint(UserPoint userPoint)
        {
            return await httpClient.PostJsonAsync<UserPoint>("api/UserPoint" , userPoint);
        }

        public async Task DeleteUserPoint(string id)
        {
            await httpClient.DeleteAsync($"api/UserPoint/{id}");
        }

        public async Task<UserPoint> GetInfo(string id)
        {
            return await httpClient.GetJsonAsync<UserPoint>($"api/UserPoint/{id}");
        }

        public async Task<IEnumerable<UserPoint>> GetInfos()
        {
            return await httpClient.GetJsonAsync<UserPoint[]>("api/UserPoint");
        }

        public async Task<UserPoint> UpdateUserPoint(UserPoint userPoint)
        {
            return await httpClient.PutJsonAsync<UserPoint>("api/UserPoint" , userPoint);
        }
    }
}
