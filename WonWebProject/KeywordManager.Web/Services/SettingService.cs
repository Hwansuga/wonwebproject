﻿using KeywordManager.Model;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace KeywordManager.Web.Services
{
    public class SettingService : ISettingService
    {
        readonly HttpClient httpClient;

        public SettingService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<Setting> AddInfo(Setting setting)
        {
            return await httpClient.PostJsonAsync<Setting>("api/Setting", setting);
        }

        public async Task DeleteInfo(int version)
        {
            await httpClient.DeleteAsync($"api/Setting/{version}");
        }

        public async Task<Setting> GetInfo(int version)
        {
            return await httpClient.GetJsonAsync<Setting>($"api/Setting/{version}");
        }

        public async Task<IEnumerable<Setting>> GetInfos()
        {
            return await httpClient.GetJsonAsync<Setting[]>("api/Setting");
        }

        public async Task<Setting> UpdateInfo(Setting setting)
        {
            return await httpClient.PutJsonAsync<Setting>("api/Setting", setting);
        }
    }
}
