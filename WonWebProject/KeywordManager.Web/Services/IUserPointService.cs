﻿using KeywordManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Web.Services
{
    public interface IUserPointService
    {
        Task<IEnumerable<UserPoint>> GetInfos();
        Task<UserPoint> GetInfo(string id);

        Task<UserPoint> UpdateUserPoint(UserPoint userPoint);
        Task<UserPoint> AddUserPoint(UserPoint userPoint);
        Task DeleteUserPoint(string id);
    }
}
